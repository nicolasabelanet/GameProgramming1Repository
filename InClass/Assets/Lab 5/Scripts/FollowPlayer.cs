using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField]
    private Camera cam;

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private Rigidbody2D playerBody;

    [SerializeField]
    private float speed = 20.0f;

    [SerializeField]
    private float maxLeadDistance = 10.0f;

    [SerializeField]
    private float maxZoomOut = 5.0f;

    private float initialSize;

    // Start is called before the first frame update
    void Awake()
    {
        initialSize = cam.orthographicSize;
    }

    // Update is called once per frame
    void Update()
    {


        Vector3 lead = playerBody.velocity;

        if (lead.magnitude > maxLeadDistance) {
            lead = lead.normalized * maxLeadDistance;
        }


        float sizeIncrease = Mathf.Clamp(playerBody.velocity.magnitude / 5.0f, 0.0f, maxZoomOut);

        float goalSize = initialSize + sizeIncrease;

        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, goalSize, Time.deltaTime * speed);

        Vector3 targetPosition = player.transform.position + lead;
        targetPosition.z = transform.position.z;

        transform.position = Vector3.Slerp(
            transform.position,
            targetPosition,
            Time.deltaTime * speed);
        
    }
}
