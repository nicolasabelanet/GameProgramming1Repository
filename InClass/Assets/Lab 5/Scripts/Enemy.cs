using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class Enemy : MonoBehaviour
{

    [SerializeField] public List<Transform> points;
    private Vector3 nextPoint;
    private int currentIndex = 0;

    [Header("Enemy Movement")]
    [SerializeField] private float moveSpeed = 0.8f;
    [SerializeField] private float patrolTime = 2.0f;
    [SerializeField] private float waitTime = 5.0f;
    private float waitEndTime;

    private bool isWaiting = false;

    private enum Direction
    {
        Right, Left
    }
    private Direction moveDirection;

    [SerializeField] private bool EnemyPatrolling;

    [Header ("Enemy Components")]
    [SerializeField] private Rigidbody2D enemyRigidbody;
    [SerializeField] private BoxCollider2D boxCollider2D;
    [SerializeField] private SpriteRenderer spriteRenderer;


    private void Move()
    {

        Vector3 dir = (nextPoint - transform.position).normalized;

        float step = Time.fixedDeltaTime * moveSpeed;

        if (Vector3.Distance(transform.position, nextPoint) > step)
        {
            enemyRigidbody.MovePosition(transform.position + (dir * step));
        }
        else
        {
            enemyRigidbody.MovePosition(nextPoint);
        }

        CheckMoveDirection(dir);

    }

    private void CheckMoveDirection(Vector3 moveDir)
    {
        if (moveDirection == Direction.Right && moveDir.x < 0)
        {
            moveDirection = Direction.Left;
            spriteRenderer.flipX = true;
        }

        if (moveDirection == Direction.Left && moveDir.x > 0)
        {
            moveDirection = Direction.Right;
            spriteRenderer.flipX = false;
        }
    }

    private IEnumerator EnemyPatrol()
    {
        while (EnemyPatrolling)
        {
            // Move the enemy
            if (!isWaiting)
            {
                Move();
            }

            if (Vector2.Distance(transform.position, nextPoint) < .1f)
            {
                nextPoint = getNextPoint();
                StartCoroutine(Wait(waitTime));
            }
           
            yield return new WaitForFixedUpdate();

        }
    }

    private IEnumerator Wait(float time)
    {

        isWaiting = true;

        waitEndTime = Time.time + time;

        while (Time.time < waitEndTime)
        {
            yield return null;
        }

        isWaiting = false;

        yield return null;
    }

    // Start is called before the first frame update
    void Start() { 

        if (points.Count > 0) {
            nextPoint = points[0].transform.position;
        }
       
        // Start the coroutine for the enemy.
        StartCoroutine(EnemyPatrol());
    }

    private Vector3 getNextPoint()
    {
        currentIndex += 1;
        currentIndex %= points.Count;
        Vector3 nextPoint = points[currentIndex].transform.position;
        return nextPoint;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("You dead.");
        }        
    }
}
