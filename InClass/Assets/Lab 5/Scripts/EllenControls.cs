using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class EllenControls : MonoBehaviour
{

    // Components
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private Rigidbody2D body;

    [SerializeField]
    private BoxCollider2D box;

    [SerializeField]
    private SpriteRenderer sprite;


    // Input variables
    private Vector2 moveInput;
    private Vector3 facing { get { return new Vector3(isRight ? 1 : -1, 0, 0); } }


    // Movement properties
    [SerializeField]
    private float maxHorizontalSpeed = 20.0f;
    [SerializeField]
    private float maxVerticalSpeed = 10.0f;
    [SerializeField]
    private float jumpForce = 10.0f;

    [SerializeField]
    private float moveAcceleration;
    private float moveForce;

    [SerializeField]
    private float frictionAmount;    


    // State variables
    private bool isRight = true;
    private bool isPushing = false;
    private bool isGrounded = true;
    private bool isOnWall = true;


    // Push detection variables
    private RaycastHit2D pushHit;


    // Ground detection variables
    private RaycastHit2D groundHit;


    // LayerMasks
    [SerializeField]
    private LayerMask enivornmentLayerMask;
    [SerializeField]
    private LayerMask boxLayerMask;


    // Lab 6
    [SerializeField]
    private PlayerInput playerInput;

    [SerializeField] private int numHearts = 4;
    public int NumHearts {get {return numHearts;} }

    public UnityEvent OnPlayerHurt;
    public UnityEvent OnPlayerHealed;


    // Occurs when the object is first constructed.
    private void Awake()
    {

        moveForce = body.mass * moveAcceleration;

        if (animator == null)
        {
            Debug.LogError("Animator is not set.");
        }

        if (body == null)
        {
            Debug.LogError("RigidBody2D is not set.");
        }

    }
    private void OnValidate()
    {
        moveForce = body.mass * moveAcceleration;
    }

    public void MoveActionPerformed(InputAction.CallbackContext context)
    {
        moveInput = new Vector2(context.ReadValue<Vector2>().x, 0);
        animator.SetBool("xInput", !Mathf.Approximately(moveInput.x, 0));
    }

    public void Jump(InputAction.CallbackContext context)
    {

        // Jump was pressed
        if (context.performed)
        {
            if (isGrounded || isOnWall)
            {
                body.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                animator.SetTrigger("Jump");
            }
        }
        else if (context.canceled)
        {
            // Cancel the jump if shes jumping
            if (body.velocity.y > 0)
            {
                body.AddForce(Vector2.down * body.velocity.y * .5f * body.mass, ForceMode2D.Impulse);
            }
        }
    }



    // Update is called once per frame
    void Update()
    {
        
    }

    private void Move(Vector2 inputDirection)
    {
        
        if (!Mathf.Approximately(inputDirection.x, 0))
        {
            applyHorizontalMovement(inputDirection);
        }

        else if (isGrounded)
        {
            applyFriction();    
        }

        applyLookDirection(inputDirection);

    }

    private void applyLookDirection(Vector2 inputDirection) {
        
        if (inputDirection.x < -0.1 && isRight)
        {
            lookLeft();
        }

        if (inputDirection.x > 0.1 && !isRight)
        {
            lookRight();
        }

    }

    private void applyFriction() {
        float amount = Mathf.Min(Mathf.Abs(body.velocity.x), Mathf.Abs(frictionAmount));
        amount *= Mathf.Sign(body.velocity.x);
        body.AddForce(Vector2.right * -amount * body.mass, ForceMode2D.Impulse);
    }

    private void applyHorizontalMovement(Vector2 inputDirection) {

        if ((inputDirection.x * body.velocity.x) < 0 && !Mathf.Approximately(body.velocity.x, 0.0f)) {
            body.AddForce(inputDirection * body.mass * Mathf.Abs(body.velocity.x), ForceMode2D.Impulse);
        }

        float speedDiff = maxHorizontalSpeed - Mathf.Abs(body.velocity.x);

        if (!Mathf.Approximately(speedDiff, 0)) {
            if (speedDiff > 0)
            {
                float accelCap = Mathf.Min(speedDiff / Time.fixedDeltaTime * body.mass, moveForce);
                body.AddForce(inputDirection * accelCap, ForceMode2D.Force);
            }

            // In this case, we're move too fast.
            else if (speedDiff < 0)
            {
                body.AddForce(new Vector2(speedDiff * Mathf.Sign(body.velocity.x), 0), ForceMode2D.Impulse);
            }

        }

    }

    private void CapVerticalSpeed() {
        float speedDiff = maxVerticalSpeed - Mathf.Abs(body.velocity.y);
        if (speedDiff < 0) {
            body.AddForce(new Vector2(0, speedDiff * Mathf.Sign(body.velocity.y) * body.mass), ForceMode2D.Impulse);
        }
    }

    private void lookLeft() {
        sprite.flipX = true;
        isRight = false;
    }

    private void lookRight() {
        sprite.flipX = false;
        isRight = true;
    }

    private void FixedUpdate()
    {
        Move(moveInput);
        CapVerticalSpeed();
        CheckRunning();
        CheckForward();
        CheckGrounded();
    }

    private void CheckGrounded()
    {
        groundHit = Physics2D.BoxCast(box.bounds.center, box.bounds.size, 0f, Vector2.down, .1f, enivornmentLayerMask);
        isGrounded = (groundHit.collider != null);
        animator.SetBool("isGrounded", isGrounded);
    }

    private void CheckForward() {

            pushHit = Physics2D.BoxCast(box.bounds.center, box.bounds.size, 0, facing, .5f, boxLayerMask);
            
            if (pushHit.collider != null)
            {
       
                if (isGrounded) {
                    
                    isOnWall = false;

                    if (!Mathf.Approximately(moveInput.x, 0) || Mathf.Abs(body.velocity.x) > 0.5f) {
                        isPushing = true;
                    }
                    else {
                        isPushing = false;
                    }
                }
                else {
                    isPushing = false;
                    isOnWall = true;
                }

            }
            else {
                isOnWall = false;
                isPushing = false;
            }

            animator.SetBool("isPushing", isPushing);

    }

    // This function updates the animator to send velocity to it.
    private void CheckRunning()
    {
        animator.SetFloat("xSpeed", Mathf.Abs(body.velocity.x));
        animator.SetFloat("ySpeed", Mathf.Abs(body.velocity.y));
        animator.SetFloat("yVelocity", body.velocity.y);
    }

    public void Hurt()
    {
        if (numHearts > 0) {
            numHearts--;
            OnPlayerHurt.Invoke();
        }
    }

    public void Heal() {
        numHearts++;
        OnPlayerHealed.Invoke();
    }

    public void OnPlayerResume() {
        SwitchCurrentActionMap("Player");
    }

    public void OnPlayerPause() {
        SwitchCurrentActionMap("UI");
    }

    private void SwitchCurrentActionMap(string mapName) {

        // Disable the current action map.
        playerInput.currentActionMap.Disable();

        // Switch to new 
        playerInput.SwitchCurrentActionMap(mapName);

        switch (mapName) {
            case "UI":
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
                break;
            case "Player":
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                break;
            
        }
    }


}
