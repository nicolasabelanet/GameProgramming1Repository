using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    [Header("Timer Configuration")]
    [SerializeField] float timerDuration;

    private bool timerStarted;
    private float endTime;

    [Header("Timer Events")]

    [SerializeField] private UnityEvent<Timer> OnTimerStarted;
    [SerializeField] private UnityEvent<Timer> OnTimerExpired;
    [SerializeField] private UnityEvent<Timer> OnTimerTick;

    // Property to see if the timer has started

    public bool TimerStarted { get { return timerStarted; } }
    public float Duration { get { return timerDuration; } set { timerDuration = value; } }


    public float TimeLeft()
    {
        float left = endTime - Time.time;

        if (left <= 0)
        {
            return 0;
        }
        else
        {
            return left;
        }
    }


    public void StartTimer()
    {
        if (!TimerStarted)
        {
            timerStarted = true;
            StartCoroutine(StartCountdown());
        }
    }

    protected virtual IEnumerator StartCountdown()
    {
        // Calculate end time

        endTime = Time.time + Duration;
        OnTimerStarted.Invoke(this);

        while(Time.time < endTime)
        {
            OnTimerTick.Invoke(this);
            yield return null;
        }

        OnTimerExpired.Invoke(this);

        yield return null;
    }
}
