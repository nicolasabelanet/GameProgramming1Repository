using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class HUDManager : MonoBehaviour
{
    private VisualElement root;
    private VisualElement heartContainer;
    private Label timerValue;
    private Button demoButton;

    [SerializeField] private Sprite heartImage;

    [SerializeField] private float initialTime = 60.0f;
    [SerializeField] private int heartSize = 128;
    [SerializeField] private EllenControls ellen;

    private void Awake()
    {
        root = GetComponent<UIDocument>().rootVisualElement;
        heartContainer = root.Q<VisualElement>("heart-container");
        timerValue = root.Q<Label>("timer-value");

        StartCoroutine(TimerCountdown());

        for (int i = 0; i < ellen.NumHearts; i++) {
            AddHeart();
        }

    }
    
    private IEnumerator TimerCountdown()
    {
        float counter = initialTime;
        while (counter > 0)
        {
            counter -= Time.deltaTime;
            counter = Mathf.Max(0f, counter);

            timerValue.text = counter.ToString();
            yield return null;
        }

    }

    public void AddHeart()
    {
        Image newHeart = new Image();
        newHeart.sprite = heartImage;
        newHeart.style.paddingTop = 5f;
        newHeart.style.paddingLeft = 0f;
        newHeart.style.paddingRight = 0f;

        newHeart.style.width = heartSize;
        newHeart.style.height = heartSize;

        heartContainer.Add(newHeart);
    }

    public void RemoveHeart()
    {
        heartContainer.RemoveAt(0);
    }
}
