using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PauseMenuManager : MonoBehaviour
{
    [SerializeField] private UIDocument pauseMenu;
    private VisualElement root;
    private Button resumeButton;
    // Start is called before the first frame update
    
    void Awake()
    {
        root = pauseMenu.rootVisualElement.Q<VisualElement>("root");
        resumeButton = root.Q<Button>("resume-game-button");

        root.style.visibility = Visibility.Hidden;
        resumeButton.clicked += GameManager.Instance.ResumeGame;
    }   

    private void OnDestroy() {
        resumeButton.clicked -= GameManager.Instance.ResumeGame;
    }

    public void OnPlayerResume() {
        root.style.visibility = Visibility.Hidden;
    }

    public void OnPlayerPause() {
        root.style.visibility = Visibility.Visible;
    }
}
