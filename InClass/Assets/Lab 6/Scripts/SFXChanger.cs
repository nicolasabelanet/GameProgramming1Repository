using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SFXChanger : MonoBehaviour
{

    [SerializeField] private AudioClip victorySFX;
    [SerializeField] private AudioClip defeatSFX;

    [SerializeField] private AudioSource audioSource;

    private void Update()
    {
        Keyboard keyboard = Keyboard.current;
        if (keyboard.digit1Key.wasPressedThisFrame)
        {
            PlaySFX(victorySFX);
        }

        else if (keyboard.digit2Key.wasPressedThisFrame)
        {
            PlaySFX(defeatSFX);
        }
    }

    private void PlaySFX(AudioClip clip)
    {
        if (clip == audioSource.clip)
            return;

        audioSource.Stop();
        audioSource.clip = clip;
        audioSource.Play();
    }
}
