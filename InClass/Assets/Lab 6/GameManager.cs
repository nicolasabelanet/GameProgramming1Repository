using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public enum GameState
    {
        Playing,
        Paused
    }

    public GameState CurrentGameState { get ; private set; }

    // Events for the game being paused.
    public UnityEvent OnGamePaused;
    public UnityEvent OnGameResumed;

    private void Awake()
    {

        // Is this the first time we have created this singleton.
        if (_instance == null)
        {
            // We're the first game manager so assign ourselves to
            // this instance.
            _instance = this;

            // Keep ourselves around between levels.
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            // Another game manager exists destroy this one.
            Destroy(this);
        }

    }

    public void PauseGame()
    {
        if (CurrentGameState != GameState.Paused) {
            CurrentGameState = GameState.Paused;
            // Adjust the time scale
            Time.timeScale = 0.0f;
            OnGamePaused.Invoke();
        }

    }


    // Resumes the game play.
    public void ResumeGame()
    {
        if (CurrentGameState != GameState.Playing) {
            CurrentGameState = GameState.Playing;
            // Adjust the time scale
            Time.timeScale = 1f;
            // Notify everyone that is listening.
            OnGameResumed.Invoke();
        }
    }


    void OnScendLoaded(Scene scene, LoadSceneMode mode)
    {
        ResumeGame();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
