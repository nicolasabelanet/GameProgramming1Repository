using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PauseMenuController : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private UIDocument pauseMenu;

    private VisualElement root;
    private Button resumeButton;
    private Button quitButton;

    #region Unity Functions

    void Awake() {
        root = pauseMenu.rootVisualElement.Q<VisualElement>("root");
        resumeButton = root.Q<Button>("resume-button");
        quitButton = root.Q<Button>("quit-button");
    }

    void Start()
    {
        root.style.visibility = Visibility.Hidden;
        resumeButton.clicked += GameManager.Instance.ResumeGame;
        quitButton.clicked += GameManager.Instance.QuitToMenu;
        SubscribeEvents();
    }   

    private void OnDestroy() {
        resumeButton.clicked -= GameManager.Instance.ResumeGame;
        quitButton.clicked -= GameManager.Instance.QuitToMenu;
        UnsubscribeEvents();
    }

    #endregion

    private void SubscribeEvents() {
        GameManager.Instance.OnGamePaused.AddListener(ShowMenu);
        GameManager.Instance.OnGameResumed.AddListener(HideMenu);
    }   

    private void UnsubscribeEvents() {
        GameManager.Instance.OnGamePaused.AddListener(ShowMenu);
        GameManager.Instance.OnGameResumed.AddListener(HideMenu);
    }

    public void HideMenu() {
        root.style.visibility = Visibility.Hidden;
    }

    public void ShowMenu() {
        root.style.visibility = Visibility.Visible;
    }

}
