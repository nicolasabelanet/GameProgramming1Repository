using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{

    [Header("Components")]
    [SerializeField] private UIDocument pauseMenu;
    [SerializeField] private CreditsMenuController creditsMenu;

    private VisualElement root;
    private Button startButton;
    private Button creditsButton;
    private Button quitButton;

    #region Unity Functions

    private void Awake() {
        root = pauseMenu.rootVisualElement.Q<VisualElement>("root");
        startButton = root.Q<Button>("start-button");
        creditsButton = root.Q<Button>("credits-button");
        quitButton = root.Q<Button>("quit-button");
    }

    void Start()
    {
        // Reset the time scale.
        Time.timeScale = 1.0f;

        // Find and delete the game manager.
        GameObject gm = GameObject.FindGameObjectWithTag("GameManager");

        if (gm != null) {
            Destroy(gm);
        }

        startButton.clicked += StartGame;
        creditsButton.clicked += creditsMenu.Stow;
        quitButton.clicked += QuitGame;
    }   

    private void OnDestroy() {
        startButton.clicked -= StartGame;
        creditsButton.clicked -= creditsMenu.Stow;
        quitButton.clicked -= QuitGame;
    }

    #endregion

    private void QuitGame() {
        Application.Quit();        
    }

    private void StartGame() {
        SceneManager.LoadScene("Level1");
    }

}
