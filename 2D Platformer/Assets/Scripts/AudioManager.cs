using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    // Way of serializing key value pairs.
    [System.Serializable]
    public class Sound {
        [SerializeField] private string name;
        [SerializeField] private AudioClip clip;
        public string Name { get {return name; } }
        public AudioClip Clip { get {return clip; } }
    }

    [Header("Components")]
    [SerializeField] private AudioSource audioSource;

    [Header("Sound")]
    [SerializeField] private List<Sound> sounds;

    // Internal way of getting index in list from the name of a sound.
    private Dictionary<string, int> mappings;

    #region Unity Functions

    void Awake()
    {
        mappings = getMappings();
    }

    private void OnValidate() {
        mappings = getMappings();
    }

    #endregion

    // Generates the mappings from name of the sound
    // to its index in the list.
    private Dictionary<string, int> getMappings() {
        
        Dictionary<string, int> mapping = new Dictionary<string, int>();

        if  (sounds != null) {
            for (int i = 0; i < sounds.Count; i++) {
                mapping.Add(sounds[i].Name, i);
            }
        }

        return mapping;

    }

    // Plays a sound from the name.
    public void PlaySound(string clipName) {
        int clipIndex = mappings[clipName];
        audioSource.clip = sounds[clipIndex].Clip;
        audioSource.Play();
    }

}
