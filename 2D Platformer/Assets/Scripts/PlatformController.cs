using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{

    [Header("Components")]
    // The list of points to move between.
    [SerializeField] private List<GameObject> points;

    // All gears attached to this platform.
    [SerializeField] private List<Gear> gears;
    [SerializeField] private Rigidbody2D rb;


    [Header("Platform Properties")]
    [SerializeField, Range(0, 100.0f)] private float speed = 2f;

    // The distance between the platform and the next point
    // to be considered equal.
    private float minDistance = .01f;
    private int currentIndex = 0;

    // The point we are currently moving towards.
    private Vector2 nextPoint;

    // Current direction.
    private Vector2 direction;

    // Public properties
    public float Speed { get { return speed; } }
    public Vector2 Direction { get { return new Vector2(direction.x, direction.y); } }
    public Vector2 Velocity { get { return direction * speed; } }

    #region Unity Functions

    void Awake()
    {
        if (points.Count > 0)
        {
            nextPoint = points[0].transform.position;
        }
        else
        {
            Debug.LogError("No points to move between.");
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdatePosition();
    }

    #endregion

    private Vector3 GetNextPoint()
    {
        currentIndex += 1;
        currentIndex %= points.Count;
        nextPoint = points[currentIndex].transform.position;
        return nextPoint;
    }

    private void UpdatePosition()
    {

        Vector2 position = rb.position;
        direction = (nextPoint - position).normalized;
        float stepSize = Time.fixedDeltaTime * speed;

        // If the distance between the current
        // position and the next point is larger
        // than the step size apply the step.
        if (Vector2.Distance(position, nextPoint) > stepSize)
        {
            rb.MovePosition(position + (direction * stepSize));
        }

        // Otherwise just move to the point.
        else
        {
            rb.MovePosition(nextPoint);
        }

        // Check to see if you are effectively at the next point.
        if (Vector2.Distance(position, nextPoint) < minDistance)
        {
            FlipGears();
            nextPoint = GetNextPoint();
        }

    }

    void FlipGears()
    {
        foreach (Gear gear in gears)
        {
            gear.Flip();
        }
    }

}
