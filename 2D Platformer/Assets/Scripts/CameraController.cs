using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header ("Components")]
    [SerializeField] private Camera cam;
    [SerializeField] private GameObject player;
    [SerializeField] private Rigidbody2D playerBody;

    [Header ("Follow Properties")]
    [SerializeField] private float speed = 20.0f;
    [SerializeField] private float maxLeadDistance = 10.0f;
    [SerializeField] private float leftBound;
    [SerializeField] private float rightBound;

    // Internal representation of the left and right bound
    // adjusted for the edges of the sceen.
    private float leftBoundAdjusted;
    private float rightBoundAdjusted;

    #region Unity Functions

    void Awake()
    {
        CalculateBounds();
    }

    private void OnValidate()
    {
        CalculateBounds();
    }

    private void Update()
    {
        UpdateCameraPosition();        
    }

    #endregion

    private void CalculateBounds()
    {
        leftBoundAdjusted = leftBound + (cam.orthographicSize * cam.aspect);
        rightBoundAdjusted = rightBound + (cam.orthographicSize * cam.aspect);
    } 

    private Vector3 CapPosition(Vector3 targetPosition, float leftBound, float rightBound)
    {
        targetPosition.x = Mathf.Max(targetPosition.x, leftBound);
        targetPosition.x = Mathf.Min(targetPosition.x, rightBound);

        return targetPosition;
    }

    // Updates the position of the camera.
    // The camera looks ahead of the player.
    void UpdateCameraPosition()
    {
        Vector3 lead = playerBody.velocity;

        if (lead.magnitude > maxLeadDistance)
        {
            lead = lead.normalized * maxLeadDistance;
        }

        Vector3 targetPosition = player.transform.position + lead;
        targetPosition.z = transform.position.z;

        targetPosition = CapPosition(targetPosition, leftBoundAdjusted, rightBoundAdjusted);

        transform.position = Vector3.Lerp(
            transform.position,
            targetPosition,
            Time.deltaTime * speed
        );

    }

}
