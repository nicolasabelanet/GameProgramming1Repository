using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class EnemyPatrol : MonoBehaviour
{

    private enum PatrolType
    {
        Time,
        EdgeDetection
    }

    private enum Direction
    {
        Right, Left
    }

    [Header("Components")]
    [SerializeField] private Animator animator;
    [SerializeField] private BoxCollider2D box;
    [SerializeField] private Rigidbody2D enemyRigidbody;
    [SerializeField] private SpriteRenderer spriteRenderer;

    [Header("Patrol Properties")]
    [SerializeField] private PatrolType patrolType = PatrolType.EdgeDetection;
    [SerializeField] private bool EnemyPatrolling;
    [SerializeField] private LayerMask stopLayerMask;

    [Header("Enemy Movement")]
    [SerializeField] private float moveSpeed = 0.8f;
    [SerializeField] private float patrolTime = 5.0f;
    [SerializeField] private float waitTime = 5.0f;
   
    [SerializeField] private Direction moveDirection;

    private Vector3 facing { get { return moveDirection == Direction.Right ? Vector3.right : Vector3.left; } }


    #region Unity Functions
    void Start()
    {
        // Start the coroutine for the enemy.
        StartPatrol();
    }
    #endregion

    // Move the enemy.
    private void Move()
    {
        float step = Time.fixedDeltaTime * moveSpeed;
        enemyRigidbody.MovePosition(transform.position + (facing * step));
        animator.SetFloat("xSpeed", moveSpeed);
    }

    // Start patrolling based on patrol type.
    private void StartPatrol() {

        switch (patrolType) {
            case PatrolType.Time:
                StartCoroutine(EnemyPatrolTime());
                break;
            case PatrolType.EdgeDetection:
                StartCoroutine(EnemyPatrolEdgeDetection());
                break;
        }

    }

    // Patrol based on edge and obstacle detection.
    private IEnumerator EnemyPatrolEdgeDetection () {
        EnemyPatrolling = true;

        while (EnemyPatrolling)
        {    
            Move();
            
            if (DetectEdge() || DetectObstacle()) {
                EnemyPatrolling = false;
            }

            yield return new WaitForFixedUpdate();
        }

        StartCoroutine(Wait());
        yield return new WaitForFixedUpdate();
    }


    // Patrol based on fixed time.
    private IEnumerator EnemyPatrolTime()
    {

        EnemyPatrolling = true;

        while (EnemyPatrolling)
        {

            float counter = 0.0f;

            while (counter < patrolTime) {
                
                Move();
                
                counter += Time.deltaTime;
                if (counter > patrolTime) {
                    EnemyPatrolling = false;
                }

                yield return new WaitForFixedUpdate();

            }
            
        }

        StartCoroutine(Wait());
        yield return new WaitForFixedUpdate();

    }

    // Check to see if there is an edge
    // or ledge in the path of the enemy.
    private bool DetectEdge() {

        float distance = (box.bounds.size.x) + .5f;

        RaycastHit2D groundHit = Physics2D.BoxCast(
            box.bounds.center + (facing * distance),
            box.bounds.size,
            0f, Vector2.down, .1f, stopLayerMask
        );

        return (groundHit.collider == null);
    }

    // Check to see if there is an obstacle
    // in the path of the enemy.
    private bool DetectObstacle() {

        RaycastHit2D obstacleHit = Physics2D.BoxCast(
            box.bounds.center,
            box.bounds.size,
            0f, facing, 1f, stopLayerMask
        );

        if (obstacleHit.collider != null) {

            Vector3 hitDir = obstacleHit.transform.position - transform.position;

            if (Vector3.Dot(hitDir, facing) > 0) {
                return true;
            }
        }

        return false;
    
    }

    // Flip the direction the enemy is facing.
    private void Flip() {
        if (moveDirection == Direction.Left) {
            moveDirection = Direction.Right;
        }
        else {
            moveDirection = Direction.Left;
        }

        spriteRenderer.flipX = !spriteRenderer.flipX;
    }

    // Wait and then patrol after
    // a given period of time.
    private IEnumerator Wait()
    {

        animator.SetFloat("xSpeed", 0.0f);

        float counter = 0.0f;

        while (counter < waitTime)
        {
            counter += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        Flip();
        StartPatrol();
        yield return new WaitForFixedUpdate();
    }

}
