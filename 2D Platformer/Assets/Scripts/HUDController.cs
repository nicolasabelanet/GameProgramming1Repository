using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class HUDController : MonoBehaviour
{
    [Header("Heart Properties")]
    [SerializeField] private Sprite heartImage;
    [SerializeField] private int heartSize = 128;

    private VisualElement root;
    private VisualElement heartContainer;

    #region Unity Functions

    private void Start()
    {
        root = GetComponent<UIDocument>().rootVisualElement;
        heartContainer = root.Q<VisualElement>("heart-container");
        SubscribeEvents();
        GetInitialHealth();
    }

    private void OnDestroy() {
        UnsubscribeEvents();
    }

    #endregion

    // Get the number of hearts the player has and adjust
    // the HUD accordingly.
    private void GetInitialHealth() {
        for (int i = 0; i < GameManager.Instance.PlayerLives; i++) {
            AddHeart();
        }
    }

    private void SubscribeEvents() {
        GameManager.Instance.OnPlayerHealed.AddListener(AddHeart);
        GameManager.Instance.OnPlayerHurt.AddListener(RemoveHeart);
    }   

    private void UnsubscribeEvents() {
        GameManager.Instance.OnPlayerHealed.RemoveListener(AddHeart);
        GameManager.Instance.OnPlayerHurt.RemoveListener(RemoveHeart);
    }

    // Add a heart to the HUD.
    public void AddHeart()
    {
        Image newHeart = new Image();
        newHeart.sprite = heartImage;
        newHeart.style.paddingTop = 5f;
        newHeart.style.paddingLeft = 0f;
        newHeart.style.paddingRight = 0f;

        newHeart.style.width = heartSize;
        newHeart.style.height = heartSize;

        heartContainer.Add(newHeart);
    }

    // Remove a heart from the HUD.
    public void RemoveHeart()
    {
        heartContainer.RemoveAt(0);
    }

}
