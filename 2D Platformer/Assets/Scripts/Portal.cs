using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] private string nextLevelName;

    #region Unity Functions
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            if (nextLevelName != null && nextLevelName != "") {
               GameManager.Instance.LoadScene(nextLevelName);
            }
            else {
                GameManager.Instance.Victory();
            }
        }
    }
    #endregion

}
