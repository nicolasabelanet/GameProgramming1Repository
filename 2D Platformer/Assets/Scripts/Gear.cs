using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gear : MonoBehaviour
{

    [Header("Gear Properties")]
    [SerializeField] private float speed = 5.0f;
    public float Speed { get { return speed; } set { speed = value; } }
    [SerializeField] private bool clockwise = false;

    #region Unity Functions

    void Update()
    {
        UpdateRotation();
    }

    #endregion

    // Update the current rotation.
    private void UpdateRotation ()
    {
        float step = (speed * 100) * Time.deltaTime;
        if (clockwise)
        {
            step *= -1;
        }

        Vector3 target = new Vector3(0, 0, transform.rotation.eulerAngles.z + step);
        transform.rotation = Quaternion.Euler(target);
    }

    public void Flip()
    {
        clockwise = !clockwise;
    }

}
