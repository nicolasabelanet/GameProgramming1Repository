using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistonController : MonoBehaviour
{

    public enum State
    {
        Extending,
        Retracting
    }

    [Header ("Components")]
    [SerializeField] Rigidbody2D body;
    [SerializeField] private List<Gear> gears;

    [Header("Piston Properties")]
    [SerializeField] private float retractionSpeed = 5.0f;
    [SerializeField] private float extensionSpeed = 5.0f;
    [SerializeField] private float extensionHeight = 5.0f;

    private Vector2 startPosition;
    private Vector2 endPosition;

    private State state = State.Extending;

    #region Unity Functions
    void Awake()
    {
        startPosition = transform.position;
        CalculateParameters();
    }

    private void OnValidate()
    {
        CalculateParameters();
    }

    void FixedUpdate()
    {
        UpdatePiston();
    }

    #endregion

    // Calculate the start and end position
    // of piston extension.
    private void CalculateParameters()
    {

        if (state == State.Extending)
        {
            SetGearSpeed(extensionSpeed);
        }
        else
        {
            SetGearSpeed(retractionSpeed);
        }

        endPosition = startPosition;
        endPosition.y += extensionHeight;
    }

    // Update the piston.
    private void UpdatePiston()
    {
        // If the piston is extending.
        if (state == State.Extending)
        {
            if (UpdatePosition(endPosition, extensionSpeed))
            {
                // The piston is done extending.
                SetGearSpeed(retractionSpeed);
                FlipGears();
                state = State.Retracting;
            }
        }

        // If the piston is retracting.
        if (state == State.Retracting)
        {
            if (UpdatePosition(startPosition, retractionSpeed))
            {
                // The piston is done retracting.
                SetGearSpeed(extensionSpeed);
                FlipGears();
                state = State.Extending;
            }
        }
    }

    private bool UpdatePosition(Vector2 targetPosition, float speed)
    {

        Vector2 position = body.position;
        Vector2 direction = (targetPosition - position).normalized;
        float stepSize = Time.fixedDeltaTime * speed;

        if (Vector2.Distance(position, targetPosition) > stepSize)
        {
            body.MovePosition(position + (direction * stepSize));
        }
        else
        {
            body.MovePosition(targetPosition);
        }

        // Returns whether or not you have reached the target position.
        if (Vector2.Distance(position, targetPosition) < .1f)
        {
            return true;
        }

        else
        {
            return false;
        }

    }

    private void FlipGears()
    {
        foreach (Gear g in gears)
        {
            g.Flip();
        }
    }

    private void SetGearSpeed(float speed)
    {
        foreach (Gear g in gears)
        {
            g.Speed = speed;
        }
    }

}
