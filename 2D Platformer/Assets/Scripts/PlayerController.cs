using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{

    public enum Direction
    {
        Right,
        Left
    }

    // Components
    [Header("Components")]
    [SerializeField] private Animator animator;
    [SerializeField] private Rigidbody2D body;
    [SerializeField] private BoxCollider2D box;
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private PlayerInput playerInput;

    [Header("Hurt Properties")]
    [SerializeField] private float damageFreezeTime = .5f;
    [SerializeField] private float damageKnockback = 5.0f;

    // Movement properties
    [Header("Movement Properties")]
    [SerializeField] private float maxHorizontalSpeed = 20.0f;
    [SerializeField] private float moveAcceleration = 81.0f;
    [SerializeField] private float frictionAmount = 1.0f;

    [SerializeField] private float maxJumpAirTime = 1.0f;
    [SerializeField] private float jumpAmount = 10.0f;
    [SerializeField] private float fallAcceleration = 10.0f;
    [SerializeField] private float maxFallSpeed = 10.0f;

    private float fallForce;
    private float moveForce;
    private float jumpForce;

    // Collision Properties
    [Header("Collision Properties")]

    [SerializeField] float groundCollisionDistance = .1f;
    [SerializeField] float groundColliderScale = .5f;
    [SerializeField] float wallCollisionDistance = .1f;
    [SerializeField] float wallColliderScale = .5f;

    [SerializeField, Tooltip("Things the player can land on and be considered grounded.")]
    private LayerMask groundedLayerMask;
    [SerializeField, Tooltip("Things the player can run in to and be hitting a wall.")]
    private LayerMask wallLayerMask;

    // State variables
    private bool isGrounded = true;
    private bool isHittingWall = false;
    private bool isJumping = false;
    private float airTime = 0.0f;
    private bool respawn = false;

    // Initial look direction.
    private Direction lookDireciton = Direction.Right;

    // Input variables
    private Vector2 moveInput;
    private Vector2 facing { get { return (lookDireciton == Direction.Right ? Vector2.right : Vector2.left); } }

    // The current platform the player is standing on.
    private PlatformController platform;

    // The point where the player will respawn.
    private Vector3 respawnPoint;


    #region Unity Functions
    private void Awake()
    {  
        fallForce = body.mass * fallAcceleration;
        moveForce = body.mass * moveAcceleration;
        jumpForce = body.mass * jumpAmount;
    }

    private void Start() {
        SubscribeEvents();
    }

    private void OnDestroy() {
        UnsubscribeEvents();
    }

    private void OnValidate()
    {
        fallForce = body.mass * fallAcceleration;
        moveForce = body.mass * moveAcceleration;
        jumpForce = body.mass * jumpAmount;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        // Check to see if the player is on a platform.
        if (collision.gameObject.tag == "Platform")
        {
            if (isGrounded)
            {
                platform = collision.gameObject.GetComponent<PlatformController>();
            }
        }

    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        // Check to see if the player has fallen off a platform.
        if (collision.gameObject.tag == "Platform")
        {
            platform = null;
        }
    }

    private void FixedUpdate()
    {

        Move(moveInput);
        CheckAirTime();
        CheckFalling();
        CapVerticalSpeed();
        CheckGrounded();
        CheckWall();
        UpdateState();
    }

    #endregion

    #region Events

    private void SubscribeEvents() {
        GameManager.Instance.OnGameOver.AddListener(SwitchToUIMap);
        GameManager.Instance.OnGamePaused.AddListener(SwitchToUIMap);
        GameManager.Instance.OnGameVictory.AddListener(SwitchToUIMap);
        GameManager.Instance.OnGameResumed.AddListener(SwitchToPlayerMap);
    }

    private void UnsubscribeEvents() {
        GameManager.Instance.OnGameOver.RemoveListener(SwitchToUIMap);
        GameManager.Instance.OnGamePaused.RemoveListener(SwitchToUIMap);
        GameManager.Instance.OnGameVictory.RemoveListener(SwitchToUIMap);
        GameManager.Instance.OnGameResumed.RemoveListener(SwitchToPlayerMap);
    }

    public void SwitchToPlayerMap() {
        SwitchCurrentActionMap("Player");
    }

    public void SwitchToUIMap() {
        SwitchCurrentActionMap("UI");
    }

    #endregion

    public void MoveActionPerformed(InputAction.CallbackContext context)
    {
        moveInput = new Vector2(context.ReadValue<Vector2>().x, 0);
    }

    public void Jump(InputAction.CallbackContext context)
    {

        // Jump was pressed
        if (context.performed)
        {

            // Ensure the platform friction
            // does not apply to character when
            // jumping.
            if (platform != null)
            {
                platform = null;
            }

            if (isGrounded)
            {
                
                isJumping = true;
                airTime = 0.0f;
                body.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                animator.SetTrigger("Jump");
            }
        }
        else if (context.canceled)
        {
            CancelJump();
        }
    }

    private void CheckAirTime() {
       if (isJumping) {        

            if (airTime < maxJumpAirTime) {
                airTime += Time.fixedDeltaTime;
            }
            else {
                CancelJump();
            }
        }
    }

    private void CancelJump() {
        isJumping = false;
        // Cancel the jump if shes jumping
        if (body.velocity.y > 0)
        {
            body.AddForce(Vector2.down * body.velocity.y * body.mass * .5f, ForceMode2D.Impulse);
        }
    }

    private void Move(Vector2 moveInput)
    {
        if (!Mathf.Approximately(moveInput.x, 0))
        {
            ApplyMovement(moveInput);
        }

        else if (isGrounded)
        {
            ApplyFriction();
        }

        CheckLookDirection(moveInput);

    }

    private void ApplyFriction()
    {

        // Friction on a platform.
        if (platform != null)
        {
            // Calculate the direction and amount of force.
            Vector2 velocityDifference = platform.Velocity - body.velocity;
            Vector2 direction = velocityDifference.normalized;

            float amount = Mathf.Min(velocityDifference.magnitude, frictionAmount * platform.Speed);
            body.AddForce(direction * body.mass * amount, ForceMode2D.Impulse);

            // Apply force so the player does not fly up on vertical
            // platforms.
            if (body.velocity.y > 0)
            {
                body.AddForce(new Vector2(0, -body.velocity.y * body.mass), ForceMode2D.Impulse);
            }

        }

        // Friction on the ground.
        else
        {
            // Calculate the amount of friction.
            float amount = Mathf.Min(Mathf.Abs(body.velocity.x), Mathf.Abs(frictionAmount));
            amount *= Mathf.Sign(body.velocity.x);

            // Apply the friction.
            body.AddForce(Vector2.right * -amount * body.mass, ForceMode2D.Impulse);
        }

    }

    private void ApplyMovement(Vector2 moveInput)
    {

        // Flip move direction of player if the input is in the opposite direction
        // of the velocity.
        if ((moveInput.x * body.velocity.x) < 0 && !Mathf.Approximately(body.velocity.x, 0))
        {
            body.AddForce(moveInput * Mathf.Abs(body.velocity.x) * 2 * body.mass, ForceMode2D.Impulse);
        }

        // Calculate how close the player is to the max speed.
        float speedDifference = maxHorizontalSpeed - Mathf.Abs(body.velocity.x);

        if (!Mathf.Approximately(speedDifference, 0))
        {

            // The player moving slower that the max speed.
            if (speedDifference > 0)
            {
                float accelerationCap = Mathf.Min(speedDifference / Time.fixedDeltaTime * body.mass, moveForce);
                body.AddForce(moveInput * accelerationCap, ForceMode2D.Force);
            }

            // In this case, we're move too fast.
            else if (speedDifference < 0)
            {
                body.AddForce(new Vector2(speedDifference * Mathf.Sign(body.velocity.x), 0), ForceMode2D.Impulse);
            }

        }

    }

    // This checks to see if the player jump is
    // starting to slow down and adds falling
    // forces.
    private void CheckFalling()
    {
        if (body.velocity.y < 2.0f)
        {
            body.AddForce(Vector2.down * fallForce, ForceMode2D.Force);
        }
    }

    // Caps the vertical speed.
    // Note - Only caps falling speed because
    // the pistons require that
    // the vertical speed be extremely high.
    private void CapVerticalSpeed()
    {
        if (body.velocity.y < 0.0f)
        {
            float speedDiff = maxFallSpeed - Mathf.Abs(body.velocity.y);
            if (speedDiff < 0)
            {
                body.AddForce(new Vector2(0, speedDiff * Mathf.Sign(body.velocity.y) * body.mass), ForceMode2D.Impulse);
            }
        }
    }


    // Ensures the sprite is looking the right direction.
    private void CheckLookDirection(Vector2 moveInput)
    {

        if (moveInput.x > 0.1 && lookDireciton == Direction.Left)
        {
            LookRight();
        }

        if (moveInput.x < -0.1 && lookDireciton == Direction.Right)
        {
            LookLeft();
        }

    }

    private void LookLeft()
    {
        sprite.flipX = true;
        lookDireciton = Direction.Left;
    }

    private void LookRight()
    {
        sprite.flipX = false;
        lookDireciton = Direction.Right;
    }

    private void CheckGrounded()
    {
        // Calculate the distance the box cast needs to be cast
        // in order to stick out from the player collider 
        // groundCollisionDistance meters.
        float distance = (box.bounds.size.y * groundColliderScale * .5f) + groundCollisionDistance;

        RaycastHit2D groundHit = Physics2D.BoxCast(
            box.bounds.center,
            box.bounds.size * groundColliderScale,
            0f, Vector2.down, distance, groundedLayerMask
        );

        isGrounded = (groundHit.collider != null);

    }

    private void CheckWall()
    {
        // Calculate the distance the box cast needs to be cast
        // in order to stick out from the player collider 
        // wallCollisionDistance meters.
        float distance = (box.bounds.size.x * wallColliderScale * .5f) + wallCollisionDistance;

        RaycastHit2D wallHit = Physics2D.BoxCast(
            box.bounds.center,
            box.bounds.size * wallColliderScale,
            0f, facing, distance, wallLayerMask
        );

        isHittingWall = (wallHit.collider != null);
    }

    // This function updates the animator to send relevant state information.
    private void UpdateState()
    {
        animator.SetBool("xInput", !Mathf.Approximately(moveInput.x, 0));

        float xSpeed = Mathf.Abs(body.velocity.x);

        // Adjust the variable to represent
        // xSpeed relative to the platform.
        if (platform != null)
        {
            xSpeed = Mathf.Abs(body.velocity.x - (platform.Velocity.x));
        }

        animator.SetFloat("xSpeed", xSpeed);
        animator.SetFloat("yVelocity", body.velocity.y);
        animator.SetBool("isGrounded", isGrounded);
        animator.SetBool("isHittingWall", isHittingWall);
    }

    private void SwitchCurrentActionMap(string mapName) {

        // Disable the current action map.
        playerInput.currentActionMap.Disable();

        // Switch to new 
        playerInput.SwitchCurrentActionMap(mapName);

        switch (mapName) {
            case "UI":
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
                break;
            case "Player":
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                break;
            
        }
    }

    // Respawns the players.
    private void Respawn() {
        respawn = false;
        transform.position = respawnPoint;
    }

    // Checks to see if the player should
    // be respawned.
    public void CheckRespawn() {
        if (respawn && GameManager.Instance.PlayerLives > 0) {
            Respawn();
        }
    }

    // Hurt the player.
    public void Hurt(Transform hazardPosition, bool knockback = true, bool respawn = false, Vector3 respawnPoint = new Vector3())
    {
        animator.SetTrigger("Hurt");

        StartCoroutine(Freeze());

        if (respawn) {
            this.respawn = true;
            this.respawnPoint = respawnPoint;
        }

        if (knockback) {
            Knockback(hazardPosition);
        }
        
        GameManager.Instance.PlayerHurt();
        
    }

    public void Pause()
    {
        GameManager.Instance.PauseGame();
    }

    public void Resume()
    {
        GameManager.Instance.ResumeGame();
    }

    // Knockback the player.
    private void Knockback(Transform hazardPosition) {
        float dirX = Mathf.Sign(transform.position.x - hazardPosition.position.x);

        Vector2 direction = new Vector2(dirX, 1.0f);
        direction.Normalize();
        direction *= damageKnockback;
        body.velocity = direction;
    }

    // Head the player.
    public void Heal() {
        GameManager.Instance.PlayerHealead();
    }

    // Freeze the player input.
    private IEnumerator Freeze() {

        float counter = 0.0f;
        playerInput.currentActionMap.Disable();

        while (counter < damageFreezeTime) {
            counter += Time.deltaTime;
            yield return null;
        }
        
        playerInput.currentActionMap.Enable();

    }

}
