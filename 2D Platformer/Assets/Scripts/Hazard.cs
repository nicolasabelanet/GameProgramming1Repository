using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour
{

    [Header("Components")]
    [SerializeField] private BoxCollider2D box;

    [Header("Hazard Properties")]
    [SerializeField] bool forceRespawn = false;
    [SerializeField] bool knockback = true;
    [SerializeField] private Vector3 respawnPoint;


    #region Unity Functions

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Player")) {

            if (forceRespawn) {
                collision.GetComponent<PlayerController>().Hurt(transform, knockback, true, respawnPoint);
            }
            else {
                collision.GetComponent<PlayerController>().Hurt(transform, knockback);
            }
        }
    }

    #endregion

}
