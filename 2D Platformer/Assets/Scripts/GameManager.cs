using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public enum GameState
    {
        Playing,
        Paused,
        GameOver,
        Victory,
        Loaded
    }

    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    // Public properties
    public GameState CurrentGameState { get { return currentGameState; } private set { currentGameState = value; } }
    public int PlayerLives { get { return playerLives; } private set { playerLives = value; } }

    [Header("Properties")]
    [SerializeField] private GameState currentGameState;
    [SerializeField] private int playerLives;

    [Header("Events")]
    public UnityEvent OnGamePaused;
    public UnityEvent OnGameResumed;
    public UnityEvent OnGameVictory;
    public UnityEvent OnGameOver;
    public UnityEvent OnPlayerHurt;
    public UnityEvent OnPlayerHealed;


    #region Unity Events
    private void Awake()
    {

        // Is this the first time we have created this singleton.
        if (_instance == null)
        {
            // We're the first game manager so assign ourselves to
            // this instance.
            _instance = this;

            // Keep ourselves around between levels.
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(this.gameObject);
        }

    }

    private void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    #endregion

    // Pause the game and set the state. Invoke all related functionality.
    public void PauseGame()
    {
        if (currentGameState != GameState.Paused) {
            currentGameState = GameState.Paused;
            // Adjust the time scale
            Time.timeScale = 0.0f;
            OnGamePaused.Invoke();
        }
    }

    // Quit to the main menu. Invoke all related functionality.
    public void QuitToMenu() {
        SceneManager.LoadScene("MainMenu");
    }

    // Set the game to GameOver. Invoke all related functionality.
    public void GameOver() {
        if (currentGameState != GameState.GameOver) {
            currentGameState = GameState.GameOver;
            // Adjust the time scale
            Time.timeScale = .3f;
            StartCoroutine(InterpolateTime(Time.timeScale, 0.0f, 2f));
            OnGameOver.Invoke();
        }
    }

    // Set the game to Victory. Invoke all related functionality.
    public void Victory() {
        if (currentGameState != GameState.Victory) {
            currentGameState = GameState.Victory;
            StartCoroutine(InterpolateTime(Time.timeScale, 0.0f, 2f));
            OnGameVictory.Invoke();
        }
    }

    // Resumes the game play. Invoke all related functionality.
    public void ResumeGame()
    {
        if (currentGameState != GameState.Playing) {
            currentGameState = GameState.Playing;
            // Adjust the time scale
            StartCoroutine(InterpolateTime(Time.timeScale, 1.0f, .5f));
            // Notify everyone that is listening.
            OnGameResumed.Invoke();
        }
    }

    // The player was hurt. Invoke all related functionality.
    public void PlayerHurt() {
        
        playerLives--;
        OnPlayerHurt.Invoke();

        if (playerLives == 0) {
            GameOver();
        }

    }

    // Interpolate the time.timeScale between a start and end value of
    // a given amount of time.
    private IEnumerator InterpolateTime(float start, float end, float duration) {

        float previousTime = Time.realtimeSinceStartup;
        float counter = 0.0f;
     
        while (counter < duration) {
            Time.timeScale = Mathf.Lerp (start, end, counter / duration);
            counter += (Time.realtimeSinceStartup - previousTime);
            previousTime = Time.realtimeSinceStartup;
            yield return null;
        }
     
        Time.timeScale = end;    
    }

    // The player was healed. Invoke all relation functionality.
    public void PlayerHealead() {
        playerLives++;
        OnPlayerHealed.Invoke();
    }

    // The scene was loaded. Set the state to loaded
    // and resume the game.
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        currentGameState = GameState.Loaded;
        ResumeGame();
    }

    public void LoadScene(string name) {
        SceneManager.LoadScene(name);
    }

}
