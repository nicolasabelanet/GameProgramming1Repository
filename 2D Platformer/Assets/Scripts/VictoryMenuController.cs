using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class VictoryMenuController : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private UIDocument gameOverMenu;

    private VisualElement root;
    private Button quitButton;

    #region Unity Functions

    void Awake() {
        root = gameOverMenu.rootVisualElement.Q<VisualElement>("root");
        quitButton = root.Q<Button>("quit-button");
    }

    void Start()
    {
        root.style.visibility = Visibility.Hidden;
        quitButton.clicked += GameManager.Instance.QuitToMenu;
        SubscribeEvents();
    }   

    private void OnDestroy() {
        quitButton.clicked -= GameManager.Instance.QuitToMenu;
        UnsubscribeEvents();
    }

    #endregion

    private void SubscribeEvents() {
        GameManager.Instance.OnGameVictory.AddListener(ShowMenu);
    }
    
    private void UnsubscribeEvents() {
        GameManager.Instance.OnGameVictory.RemoveListener(ShowMenu);
    }

    public void ShowMenu() {
        root.style.visibility = Visibility.Visible;
    }

}
