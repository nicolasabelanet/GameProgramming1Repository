using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObjectController : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private AudioSource audioSource;

    [Header("Object Properties")]
    [Tooltip("Minimum amount of force to trigger a sound.")]
    [SerializeField] private float minimumForce = 5.0f;

    #region Unity Functions
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.relativeVelocity.magnitude > minimumForce)
            audioSource.Play();
    }
    #endregion

}
