using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateController : MonoBehaviour
{

    [Header("StateControl")]
    [Tooltip("The state that our context is in.")]
    public State currentState;

    [Tooltip("Wether or not this FSM is active.")]
    public bool isActive;

    // Generally, shared state goes here
    [Tooltip("The Nav Mesh Agent associated with our entity.")]
    public NavMeshAgent navMeshAgent;


    // Going home state
    [Tooltip("The target")]
    public Transform homeWaypoint;


    // Look decision
    [Tooltip("Eyes we are looking from.")]
    public Transform AIEyes;
    [Tooltip("Area we can see.")]
    public float lookRadius;
    [Tooltip("Distance the ai can see.")]
    public float lookRange;

    [Tooltip("Object we are chasing.")]
    public Transform chaseTarget;

    [SerializeField]
    private Color lookGizmoColor;

    public float counter;


    private void Setup()
    {
        counter = 0;
    }

    private void Awake()
    {
        Setup();
    }

    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            // Update our current state.
            currentState.UpdateState(this);
        }
    }

    public void TransitionToState(State nextState)
    {
        // Only transition if it's a new state
        if (nextState != currentState)
        {
            // Call the exit state function.
            OnExitState();

            // Transition to the new state.
            currentState = nextState;

            // Call the enter state
            OnEnterState();
        }
    }

    private void OnEnterState()
    {
        currentState.EnterState(this);
    }

    private void OnExitState()
    {
        currentState.ExitState(this);
    }

    public void OnDrawGizmos()
    {
        // Draw the look location
        Gizmos.color = lookGizmoColor;
        Gizmos.DrawWireSphere(AIEyes.position + AIEyes.forward * lookRange, lookRadius);
    }

}
