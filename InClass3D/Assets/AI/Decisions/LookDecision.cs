using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Look", fileName = "Look Decision")]
public class LookDecision : Decision
{

    [SerializeField] private LayerMask characterLayerMask;

    public override bool Decide(StateController controller)
    {
        bool targetVisible = Look(controller);
        return targetVisible;
    }

    private bool Look(StateController controller)
    {
        RaycastHit hit;

        if (Physics.SphereCast(
                controller.AIEyes.position,
                controller.lookRadius,
                controller.AIEyes.forward,
                out hit,
                controller.lookRange,
                characterLayerMask,
                QueryTriggerInteraction.Ignore))
        {
            controller.chaseTarget = hit.transform;
            return true;
        }

        return false;

    }

}
