using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Selection", fileName = "Selection Decision")]
public class SelectorDecision : Decision
{

    [SerializeField] private List<Decision> decisions;


    public override bool Decide(StateController controller)
    {
        foreach (Decision d in decisions)
        {
            if (d.Decide(controller)){
                return true;
            }
        }

        return false;
    }

}
