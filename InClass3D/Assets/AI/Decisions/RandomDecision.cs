using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Random", fileName = "Random Decision")]
public class RandomDecision : Decision
{
    [SerializeField]
    private float minTime = 10;
    [SerializeField]
    private float maxTime = 15;
    private float timeLimit;

    private void OnEnable()
    {
        timeLimit = Random.Range(minTime, maxTime);
    }

    public override bool Decide(StateController controller)
    {

        controller.counter += Time.deltaTime;

        if (controller.counter > timeLimit)
        {
            return true;
        }

        return false;

    }

}
