using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "PluggableAI/Actions/GoHome", fileName = "Go Home")]
public class GoHomeAction : Action
{
    public override void Act(StateController controller)
    {
        // Call our internal go home function
        GoHome(controller);
    }

    private void GoHome(StateController controller)
    {
        controller.navMeshAgent.destination = controller.homeWaypoint.position;
        controller.navMeshAgent.isStopped = false;
    }

}
