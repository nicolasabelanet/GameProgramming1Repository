using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : BaseMovement
{
    
    [Header("Ground Movement")]
    [Tooltip("The speed that this character accelerates at")]
    [SerializeField] private float maxWalkSpeed = 3.5f;
    [SerializeField] private float maxRunSpeed = 5f;
    [SerializeField] private float moveAcceleration = 60;
    [SerializeField] private bool isRunning;
    [SerializeField] private bool lockJump = false;

    [Tooltip("The max speed of this character")]
    [SerializeField] private float maxSpeed = 7.0f;

    [SerializeField] private Vector3 cameraAdjustedInputDirection;
    [SerializeField] private Rigidbody characterRigidbody;
    [SerializeField] private float rotationSpeed = 10.0f;


    [Header("Ground Check")]
    [Tooltip("Maximum distance below player to be considered grounded.")]
    [SerializeField] private float groundCheckDistance = 0.1f;
    [SerializeField] private LayerMask environmentMask;
    [SerializeField] private bool isGrounded;
    [SerializeField] private float groundDistance = 100000.0f;
     
    [Header("Air Movement")]
    [SerializeField] private float maxVerticalMoveSpeed = 25.0f;
    [SerializeField] private float jumpForce = 7.0f;
    [SerializeField] private float jumpDelay = .5f;
    [SerializeField] private float maxFallSpeed = 0.0f;

    [Header("Components")]
    [SerializeField] private CapsuleCollider capsule;
    [SerializeField] private Animator animator;
    [SerializeField] private float referenceRunningSpeed = 4.8f;
    [SerializeField] private float referenceRollingSpeed = 2.7f;
    [SerializeField] private float referenceWalkingSpeed = 2.7f;

    [Tooltip("Character Mesh")]
    [SerializeField] private Transform characterModel;

    private Coroutine maxSpeedCoroutine;

    

    private bool wasGroundedLastFrame;

    private void CheckGrounded()
    {
        // Store is grounded from the last frame.
        wasGroundedLastFrame = isGrounded;

        // Use an overlappign sphere check to see if we are grounded.
        Vector3 origin = transform.position + (Vector3.up * (capsule.radius - groundCheckDistance));

        // Query physics engine for collisions
        Collider[] overlappedCollider = Physics.OverlapSphere(origin, capsule.radius * .95f, environmentMask, QueryTriggerInteraction.Ignore);

        // Determine if we're grounded
        isGrounded = (overlappedCollider.Length > 0);

        // Set our animator here!
    }

    private void CheckGroundDistance()
    {
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity, environmentMask))
        {
            groundDistance = hit.distance;
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * hit.distance, Color.yellow);
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * 1000, Color.white);
        }
    }
        
    #region Base Movement Functions
    
    private void MoveCharacter() {
        if (cameraAdjustedInputDirection != Vector3.zero) {
            animator.SetBool("hInput", true);
            characterRigidbody.AddForce(
                cameraAdjustedInputDirection * moveAcceleration * characterRigidbody.mass, ForceMode.Force);
        }
        else
        {
            animator.SetBool("hInput", false);

        }

    }

    private Vector3 GetHorizontalRBVelocity()
    {
        return new Vector3(characterRigidbody.velocity.x, 0.0f, characterRigidbody.velocity.z);
    }

    private void LimitVelocity()
    {
        // Get our horizontal velocity
        Vector3 currentVelocity = GetHorizontalRBVelocity();

        // Get our max velocity
        float maxAllowedVelocity = GetMaxAllowedVelocity();

        
        if (currentVelocity.sqrMagnitude > (maxAllowedVelocity * maxAllowedVelocity))
        {
            // Use an impulse force to counteract the speed of this player
            Vector3 counteractDirection = currentVelocity.normalized * -1.0f;
            float counteractAmount = currentVelocity.magnitude - maxAllowedVelocity;

            characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
        }

        // Now check for falling/jumping
        if (!isGrounded)
        {
            if (Mathf.Abs(characterRigidbody.velocity.y) > maxVerticalMoveSpeed)
            {
                // Use an impulse to counteract the speed.
                Vector3 counteractDirection = Vector3.up * Mathf.Sign(characterRigidbody.velocity.y) * -1f;

                float counteractAmount = Mathf.Abs(characterRigidbody.velocity.y) - maxVerticalMoveSpeed;

                characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
            }
        }

    }

    private float GetMaxAllowedVelocity()
    {
        return maxSpeed * cameraAdjustedInputDirection.magnitude;
    }

    override public void Move(Vector3 moveDirection) {
        
        cameraAdjustedInputDirection = moveDirection;
    }

    override public void Jump()
    {

        if (isGrounded && !lockJump)
        {
            animator.SetTrigger("Jump");
            Invoke("ApplyJump", jumpDelay);
        }
    }

    override public void Run()
    {
        if (maxSpeedCoroutine != null)
        {
            StopCoroutine(maxSpeedCoroutine);
        }

        maxSpeedCoroutine = StartCoroutine(interpolateMaxSpeed(maxSpeed, maxRunSpeed, .3f));
    }

    override public void Walk()
    {
        if (maxSpeedCoroutine != null)
        {
            StopCoroutine(maxSpeedCoroutine);
        }

        maxSpeedCoroutine = StartCoroutine(interpolateMaxSpeed(maxSpeed, maxWalkSpeed, .3f));
    }

    private IEnumerator interpolateMaxSpeed(float start, float end, float time)
    {
        float counter = 0;

        float distance = end - start;

        while (counter < time)
        {
            maxSpeed = Mathf.Lerp(start, end, counter / time);
            counter += Time.deltaTime;
            yield return null;
        }

        maxSpeed = end;
        yield return null;
    }

    public void LockJump()
    {
        lockJump = true;
    }

    public void UnlockJump()
    {
        lockJump = false;
    }

    private void ApplyJump()
    {
        characterRigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }

    override public void JumpCancel()
    {
        // Cancel the jump if shes jumping
        if (characterRigidbody.velocity.y > 0)
        {
            characterRigidbody.AddForce(Vector2.down * characterRigidbody.velocity.y * characterRigidbody.mass *.5f, ForceMode.Impulse);
        }
    }

    override public void RotateCharacter()
    {
        if (cameraAdjustedInputDirection != Vector3.zero)
        {
            characterModel.forward = Vector3.Slerp(characterModel.forward, cameraAdjustedInputDirection.normalized, Time.fixedDeltaTime * rotationSpeed);
        }
    }

    #endregion

    #region Unity Functions

    protected override void Awake()
    {
        maxSpeed = maxWalkSpeed;
    }

    // Update is called once per frame
    override protected void Update()
    {
        
    }

    override protected void FixedUpdate()
    {
        MoveCharacter();
        CheckGrounded();
        CheckGroundDistance();
        LimitVelocity();
        RotateCharacter();
        UpdateState();
    }

    private float CalculateRunningSpeedMultiplier()
    {
        Vector3 hSpeed = characterRigidbody.velocity;
        hSpeed.y = 0;

        float multiplier = hSpeed.magnitude / referenceRunningSpeed;

        multiplier = Mathf.Max(.5f, multiplier);

        return multiplier;
    }

    private float CalculateJumpSpeedMultiplier()
    {

        if (Mathf.Abs(characterRigidbody.velocity.y) > 1)
        {
            return 1 / Mathf.Abs(characterRigidbody.velocity.y);
        }

        return 1f;
        
    }



    private float CalculateRollingSpeedMultiplier()
    {
        float speed = characterRigidbody.velocity.magnitude;

        float multiplier = speed / referenceRollingSpeed;

        return multiplier;
    }

    private float CheckMaxFallSpeed()
    {
        if (isGrounded || characterRigidbody.velocity.y > 0f)
        {
            maxFallSpeed = 0.0f;
        }

        return Mathf.Max(maxFallSpeed, -characterRigidbody.velocity.y);
    }


    private void UpdateState()
    {

        Vector3 hSpeed = characterRigidbody.velocity;
        hSpeed.y = 0;

        animator.SetFloat("hSpeed",  hSpeed.magnitude);
        animator.SetFloat("vSpeed", Mathf.Abs(characterRigidbody.velocity.y));
        animator.SetFloat("vVelocity", characterRigidbody.velocity.y);
        animator.SetBool("isGrounded", isGrounded);
        animator.SetFloat("groundDistance", groundDistance);
        animator.SetFloat("jumpSpeedMultiplier", CalculateJumpSpeedMultiplier());

        if (!isGrounded)
        {
            animator.SetFloat("maxFallSpeed", CheckMaxFallSpeed());

        }

        if (!isGrounded) {
            animator.SetFloat("rollingSpeedMultiplier", CalculateRollingSpeedMultiplier());
        }

        animator.SetFloat("runningSpeedMultiplier", CalculateRunningSpeedMultiplier());
    }

    #endregion
}
