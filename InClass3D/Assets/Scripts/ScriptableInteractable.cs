using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableInteractable : MonoBehaviour, IInteractable
{
    [Tooltip("The InteractBehavior that defines the behavior to execute when interacted with")]
    [SerializeField] private InteractBehavior interactBehavior;

    // Implements the Interact() function from the IInteractable interface
    public void Interact(PlayerInteractManager playerInteractManager, PlayerController playerController)
    {
        // Tell our InteractBehavior ScriptableObject to do its Interact functionality
        interactBehavior.Interact();
    }
}
