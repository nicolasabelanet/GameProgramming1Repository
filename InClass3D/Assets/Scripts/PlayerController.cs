using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{

    [SerializeField] private PlayerInteractManager pim;

    [Header("Player Input")]
    [Tooltip("The movement input from our player")]
    [SerializeField] private Vector2 movementInput;

    [Tooltip("The movement input that's aligned with the camera direction")]
    [SerializeField] private Vector3 cameraAdjustedInputDirection;

    private PlayerInputActions playerInputActions;

    [Header("Camera")]
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private Transform cameraOrientation;

    [Header("Component/Object reference")]
    [Tooltip("Reference to our movement component (drag it here)")]
    [SerializeField] private BaseMovement characterMovement;

    private void RotateCameraAndCharacter()
    {
        // Rotate the orientation to match the camera's orientation.
        Vector3 basicViewDir = transform.position - new Vector3(cameraTransform.position.x, transform.position.y, cameraTransform.position.z);

        // Now set the object's forward direction.
        cameraOrientation.forward = basicViewDir.normalized;

        // Now rotate the character.
        characterMovement.RotateCharacter();
    }

    private void CalculateCameraRelativeInput() {
        cameraAdjustedInputDirection = cameraOrientation.forward * movementInput.y + cameraOrientation.right * movementInput.x;

        // Possibly normalize if our vector is too big
        if (cameraAdjustedInputDirection.sqrMagnitude > 1) {
            cameraAdjustedInputDirection = cameraAdjustedInputDirection.normalized;
        }
    }

    private void MoveActionPerformed(InputAction.CallbackContext context) {
        
        // Get the user input.
        movementInput = context.ReadValue<Vector2>();

        // Get the relative input direction.
        CalculateCameraRelativeInput();

        // Now actually move the character using the movement component.
        characterMovement.Move(cameraAdjustedInputDirection);
    }

    private void JumpActionPerformed(InputAction.CallbackContext context)
    {
        characterMovement.Jump();
    }

    private void JumpCancelActionPerformed(InputAction.CallbackContext context)
    {
        characterMovement.JumpCancel();
    }

    private void RunActionPerformed(InputAction.CallbackContext context)
    {
        characterMovement.Run();
    }

    private void RunActionCancelled(InputAction.CallbackContext context)
    {
        characterMovement.Walk();
    }

    private void SubscribeInputActions() {
        playerInputActions.Player.Move.started += MoveActionPerformed;
        playerInputActions.Player.Move.performed += MoveActionPerformed;
        playerInputActions.Player.Move.canceled += MoveActionPerformed;
        playerInputActions.Player.Jump.started += JumpActionPerformed;
        playerInputActions.Player.Jump.canceled += JumpCancelActionPerformed;
        playerInputActions.Player.Run.started += RunActionPerformed;
        playerInputActions.Player.Run.canceled += RunActionCancelled;
        playerInputActions.Player.Interact.started += InteractionPerformed;
        // Subscribe to rest of the actions!
    }

    private void UnsubscribeInputActions() {
        playerInputActions.Player.Move.started -= MoveActionPerformed;
        playerInputActions.Player.Move.performed -= MoveActionPerformed;
        playerInputActions.Player.Move.canceled -= MoveActionPerformed;
    }

    private void InteractionPerformed(InputAction.CallbackContext context)
    {
        pim.Interact();
    }

    private void SwitchActionMap(string mapName) {
        switch(mapName) {
            case "Player":
                playerInputActions.UI.Disable();
                playerInputActions.Player.Enable();
                break;
            case "UI":
                playerInputActions.Player.Disable();
                playerInputActions.UI.Enable();
                break;
        }
    }

    private void Awake() {
        playerInputActions = new PlayerInputActions();
        SubscribeInputActions();
        SwitchActionMap("Player");
    }

    private void OnDestroy() {
        UnsubscribeInputActions();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        // Call our rotation for the camera orientation object and player
        RotateCameraAndCharacter();


        CalculateCameraRelativeInput();
        // Calculate the new relative input.
    }

    private void FixedUpdate()
    {
        characterMovement.Move(cameraAdjustedInputDirection);
    }
}
