using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Audio/RandomAudioCollection", fileName = "RandomAudioCollection")]
public class RandomAudioCollection : ScriptableObject
{
    [Tooltip("The list of all of the audioclips to choose from")]
    [SerializeField] private List<AudioClip> listOfClips;
    
    // Pick a random clip from our list of clips, and play it on the given audioSource
    public void PlayRandomAudioClip(AudioSource audioSource)
    {
        //If there is at least one clip in the list
        if(listOfClips.Count > 0)
        {
            //Pick a random audio clip and set that as the audioSource's clip
            audioSource.clip = listOfClips[Random.Range(0, listOfClips.Count)];

            //Tell the audio source to play the sound
            audioSource.Play();
        }
    }
}
