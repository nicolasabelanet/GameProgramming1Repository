using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMaterialScript : MonoBehaviour
{
    [Tooltip("The material we are acessing (drag it here)")]
    [SerializeField] private Material material;

    private int blendID;

    private void Awake()
    {
        blendID = Shader.PropertyToID("_Blending");   
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        material.SetFloat(blendID, Time.time % 1);
    }
}
