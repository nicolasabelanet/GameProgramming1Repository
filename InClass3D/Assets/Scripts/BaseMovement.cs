using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMovement : MonoBehaviour
{

    virtual public void Move(Vector3 moveDirection) {
        throw new System.NotImplementedException();
    }

    virtual public void RotateCharacter() {
        throw new System.NotImplementedException();
    }

    virtual public void Jump() {
        throw new System.NotImplementedException();
    }

    virtual public void JumpCancel() {
        throw new System.NotImplementedException();
    }

    virtual public void Run()
    {
        throw new System.NotImplementedException();
    }

    virtual public void Walk()
    {
        throw new System.NotImplementedException();
    }

    #region Unity Functions

    virtual protected void Awake() {

    }

    virtual protected void Start() {

    }

    virtual protected void Update() {

    }

    virtual protected void FixedUpdate() {

    }

    #endregion

}
