using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightPulsing : MonoBehaviour
{

    [SerializeField] private Material material;
    [SerializeField] private float period;
    [SerializeField] private float amplitude;
    [SerializeField] private float intensity;
    [SerializeField] float angle = 0;

    //[ColorUsage(true,true)]
    //[SerializeField] private Color emissiveColor = Color.red;
    [SerializeField] private Vector4 emissiveColor = Color.red;
    [SerializeField] private Vector4 emissiveResult = Color.red;
    int emissiveProperty;

    private void Start()
    {
        emissiveProperty = Shader.PropertyToID("_Emission");
    }

    // Update is called once per frame
    void Update()
    {

        angle += (5 * Time.deltaTime);

        if (angle > 360)
        {
            angle -= 360;
        }

        intensity = Mathf.Sin(angle / period) * (amplitude / 2);
        intensity += amplitude / 2;
        emissiveResult = emissiveColor * intensity;
        material.SetColor(emissiveProperty, emissiveResult);
        
    }
}
