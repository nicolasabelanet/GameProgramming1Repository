using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingBall : MonoBehaviour, IInteractable
{

    [SerializeField] private AudioSource source;

    public enum InteractionType {
        PlaySound,
        PrintMessage,
        MoveUp
    }

    [SerializeField] private InteractionType interactionType = InteractionType.PlaySound;

    [SerializeField] private Animator animator;

    public void Interact(PlayerInteractManager pim, PlayerController pc)
    {
        if (interactionType == InteractionType.PlaySound)
        {
            source.Play();
        }

        else if (interactionType == InteractionType.PrintMessage)
        {
            Debug.Log("A message was printed.");
        }

        else if (interactionType == InteractionType.MoveUp) {
            transform.position = transform.position + (Vector3.up * 5);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
