using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInteractManager : MonoBehaviour
{

    // A list of all objects we can currently interact with.
    List<GameObject> interactableObjects = new List<GameObject>();

    [Tooltip("Event called when we go from 0 to at least 1 interactable object in range.")]
    public UnityEvent OnInteractableExist;

    [Tooltip("Event called when we go form some number to 0 interactables in range.")]
    public UnityEvent OnInteractablesDoNotExist;

    [Tooltip("The player controller on the player (drag it here).")]
    [SerializeField] private PlayerController playerController;

    private void TrackObject(GameObject objectToTrack)
    {
        // Add the object to our list of tracked objects.
        interactableObjects.Add(objectToTrack);


        // Interactables exist, so let everyone know.
        if (interactableObjects.Count == 1)
        {
            OnInteractableExist.Invoke();
        }

    }

    public void UntrackObject(GameObject trackedObject)
    {

        if (interactableObjects.Contains(trackedObject))
        {
            interactableObjects.Remove(trackedObject);

            // See if we hit 0, let folks know
            if (interactableObjects.Count == 0)
            {
                OnInteractablesDoNotExist.Invoke();
            }
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        // if an interactable enters our trigger are
        if (other.CompareTag("Interactable"))
        {
            // Then track it!
            TrackObject(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Interactable"))
        {
            UntrackObject(other.gameObject);
        }
    }

    public void Interact() { 

        while (interactableObjects.Count > 0 && interactableObjects[0] == null)
        {
            UntrackObject(interactableObjects[0]);
        }
    
        if (interactableObjects.Count > 0)
        {
            // Interact only with the first one
            interactableObjects[0].GetComponent<IInteractable>().Interact(
                this,
                playerController
            );

        }

    }

}
