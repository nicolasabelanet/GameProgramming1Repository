using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class IntReference
{

    [SerializeField] private bool useConstant = true;
    [SerializeField] private int constant = 1;
    [SerializeField] private IntVariable intVariable;
    public int Value { get
        {
            return useConstant ? constant : intVariable.Value;
        } }

}
