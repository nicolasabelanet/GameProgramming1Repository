using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTransformVariable : MonoBehaviour
{
    [SerializeField] private TransformVariable transformVariable;


    // Start is called before the first frame update
    void Awake()
    {
        transformVariable.Value = transform;
    }

}
