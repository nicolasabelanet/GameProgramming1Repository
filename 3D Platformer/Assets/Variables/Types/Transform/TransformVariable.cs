using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variables/TransformVariable")]
public class TransformVariable : ScriptableObject
{

    [SerializeField] private Transform defaultTransform;

    private Transform _value;

    [property: SerializeField]
    public Transform Value {

        get
        {
            if (_value == null)
            {
                return defaultTransform;
            }
            else
            {
                return _value;
            }
        }

        set
        {
            _value = value;
        }

    }



}
