using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TransformReference 
{
    [SerializeField] private TransformVariable transformVariable;
    public Transform Value { get { return transformVariable.Value; } }
}
