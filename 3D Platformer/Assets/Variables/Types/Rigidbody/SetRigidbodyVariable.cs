using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRigidbodyVariable : MonoBehaviour
{
    [SerializeField] private RigidbodyVariable rigidbodyVariable;
    [SerializeField] private Rigidbody rb;

    // Start is called before the first frame update
    void Awake()
    {
        rigidbodyVariable.Value = rb;
    }

}
