using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RigidbodyReference 
{
    [SerializeField] private RigidbodyVariable rigidbodyVariable;
    public Rigidbody Value { get { return rigidbodyVariable.Value; } }
}
