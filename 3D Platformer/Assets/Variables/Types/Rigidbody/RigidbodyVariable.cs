using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variables/RigidbodyVariable")]
public class RigidbodyVariable : ScriptableObject
{

    [SerializeField] private Rigidbody defaultRigidbody;

    private Rigidbody _value;

    [property: SerializeField]
    public Rigidbody Value {

        get
        {
            if (_value == null)
            {
                return defaultRigidbody;
            }
            else
            {
                return _value;
            }
        }

        set
        {
            _value = value;
        }

    }


}
