using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Patrol/SetNextWaypoint", fileName = "Set Next Waypoint Patrol Action")]
public class PatrolSetNextWaypointAction : Action
{

    public override void Act(StateController controller)
    {
        SetNextWaypoint(controller);
    }

    private void SetNextWaypoint(StateController controller)
    {
        controller.patrolCurrentWaypointIndex += 1;
        controller.patrolCurrentWaypointIndex = controller.patrolCurrentWaypointIndex % controller.patrolWaypoints.Count;
        controller.currentWaypoint = controller.patrolWaypoints[controller.patrolCurrentWaypointIndex];
    }
}
