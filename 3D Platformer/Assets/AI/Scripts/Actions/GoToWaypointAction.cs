using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "PluggableAI/Actions/GoToWaypoint", fileName = "Go To Waypoint Action")]
public class GoToWaypointAction: Action
{

    public override void Act(StateController controller)
    {
        GoTo(controller);
    }

    private void GoTo(StateController controller)
    {
        controller.navMeshAgent.destination = controller.currentWaypoint;
        controller.navMeshAgent.isStopped = false;
    }

}
