using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Wander/SetNextWaypoint", fileName = "Wander Set Next Waypoint Action")]
public class WanderSetNextWaypointAction : Action
{

    public override void Act(StateController controller)
    {
        SetNextWaypoint(controller);
    }

    private void SetNextWaypoint(StateController controller)
    {
        Vector3 result = GetPoint(controller);
        controller.currentWaypoint = result;
    }

    private Vector3 GetPoint(StateController controller)
    {
        NavMeshHit hit;

        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = controller.wanderHome + Random.insideUnitSphere * controller.wanderDistance;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
            {
                return hit.position;
            }
        }

        NavMesh.SamplePosition(controller.wanderHome, out hit, 1.0f, NavMesh.AllAreas);
        return hit.position;

    }
}
