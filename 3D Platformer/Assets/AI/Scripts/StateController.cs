using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateController : MonoBehaviour
{

    [Header("StateControl")]
    [Tooltip("The state that our context is in.")]
    public State currentState;

    // Generally, shared state goes here
    [Header("Navigation Properties")]
    public NavMeshAgent navMeshAgent;
    public EnemyController enemyController;
    public Vector3 currentWaypoint;

    [Header("Attack Player Decision")]
    public float attackCooldown = 5.0f;
    public float timeSinceAttack = 0.0f;
    public float attackRange = 15f;
    public bool canAttack = true;

    [Header("Look Decision")]
    [Tooltip("Eyes we are looking from.")]
    public Transform AIEyes;
    [Tooltip("Area we can see.")]
    public float lookRadius;
    [Tooltip("Distance the ai can see.")]
    public float lookRange;
    public Vector3 lookTargetPosition;

    [HideInInspector] public int stepsSinceSeen = 1000;

    // Strictly for visualization
    [Header("Wander State")]
    public Vector3 wanderHome;
    public float wanderDistance;

    [Header("Chase Player State")]
    [Tooltip("Object we are chasing.")]
    public TransformReference chaseTarget;

    [Header("Patrol State")]
    [SerializeField] private List<Vector3> localPatrolWaypoints;
    [HideInInspector] public List<Vector3> patrolWaypoints;
    [HideInInspector] public int patrolCurrentWaypointIndex = 0;

    [Header("Timer")]
    public float counter;

    [Header("State Information")]
    [Tooltip("Wether or not this FSM is active.")]
    public bool isActive;


    [Header("Debug Visualization")]
    [SerializeField] private Color currentWaypointColor;

    [SerializeField] private Color lookRangeColor;
    [SerializeField] private Color lookRayColor;
    [SerializeField] private Color lookTargetColor;

    [SerializeField] private Color patrolWaypointsColor;

    public Color wanderHomeColor;


    private void Setup()
    {
        counter = Random.Range(0, 3f);
        CreateWorldWaypoints();
        SetHomewayPoint();
    }
    private void SetHomewayPoint()
    {
        wanderHome = SampleMesh(transform.position);
    }

    private void Awake()
    {
        Setup();
    }

    private void OnValidate()
    {
        SetHomewayPoint();
        CreateWorldWaypoints();
    }

    private Vector3 SampleMesh(Vector3 position)
    {
        NavMeshHit hit;
        NavMesh.SamplePosition(position, out hit, 2.0f, NavMesh.AllAreas);
  
        return hit.position;
    }

    private void CreateWorldWaypoints()
    {

        patrolWaypoints = new List<Vector3>( new Vector3[localPatrolWaypoints.Count] );

        for (int i = 0; i < patrolWaypoints.Count; i++)
        {
            
            patrolWaypoints[i] = SampleMesh(localPatrolWaypoints[i] + transform.position);
            localPatrolWaypoints[i] = patrolWaypoints[i] - transform.position;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (currentState != null)
        {
            currentState.EnterState(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            // Update our current state.
            currentState.UpdateState(this);
        }
    }

    public void TransitionToState(State nextState)
    {
        // Only transition if it's a new state
        if (nextState != currentState)
        {
            // Call the exit state function.
            OnExitState();

            // Transition to the new state.
            currentState = nextState;

            // Call the enter state
            OnEnterState();
        }
    }

    private void OnEnterState()
    {
        currentState.EnterState(this);
    }

    private void OnExitState()
    {
        currentState.ExitState(this);
    }

    public void OnDrawGizmosSelected()
    {
        // Draw the look location
        Gizmos.color = lookRangeColor;
        Gizmos.DrawWireSphere(AIEyes.position, lookRange);


        for(int i = 0; i < patrolWaypoints.Count; i++)
        {
            if (patrolWaypoints[i] != currentWaypoint) { 
                Gizmos.color = patrolWaypointsColor;
                Gizmos.DrawWireSphere(patrolWaypoints[i], .5f);
            }

        }
         
        Gizmos.color = currentWaypointColor;
        Gizmos.DrawWireSphere(currentWaypoint, .5f);

        Gizmos.color = lookRayColor;
        Gizmos.DrawRay(AIEyes.position, AIEyes.forward * 4.0f);

        Gizmos.color = lookTargetColor;
        Gizmos.DrawLine(AIEyes.position, lookTargetPosition);
        Gizmos.DrawWireSphere(lookTargetPosition, lookRadius);

        Gizmos.color = wanderHomeColor;
        Gizmos.DrawWireSphere(wanderHome, .5f);
    }

}
