using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Wait", fileName = "Wait Decision")]
public class WaitDecision : Decision
{
    [SerializeField]
    private float waitTime = 10;
   
    public override bool Decide(StateController controller)
    {

        controller.counter += Time.deltaTime;

        if (controller.counter > waitTime)
        {
            controller.counter = 0;
            return true;
        }

        return false;

    }

}
