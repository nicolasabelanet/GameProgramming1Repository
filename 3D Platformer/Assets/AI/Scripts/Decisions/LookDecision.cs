using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Look", fileName = "Look Decision")]
public class LookDecision : Decision
{

    [SerializeField] private LayerMask LookMask;

    private RaycastHit playerHit;

    public override bool Decide(StateController controller)
    {
        bool targetVisible = Look(controller);
        return targetVisible;
    }


    private void LookAtPlayer(StateController controller)
    {
        float distance = Vector3.Distance(controller.AIEyes.position, controller.chaseTarget.Value.position);

        if (distance > controller.lookRange)
        {
            return;
        }

        Vector3 direction = (controller.chaseTarget.Value.position - controller.AIEyes.position).normalized;

        if (distance > 15f)
        {
            if (Vector3.Dot(direction, controller.AIEyes.forward) < .2f)
            {
                return;
            }
        }

        if (Physics.SphereCast(controller.AIEyes.position, controller.lookRadius, direction, out playerHit, controller.lookRange, LookMask))
        {
            if (playerHit.collider.CompareTag("Player"))
            {
                controller.lookTargetPosition = playerHit.point;
                controller.stepsSinceSeen = 0;
            }
        }

    }


    private bool Look(StateController controller)
    {

        LookAtPlayer(controller);

        // The player has been lost for around 3 seconds.
        if (controller.stepsSinceSeen < 300)
        {
            controller.stepsSinceSeen += 1;
            return true;
        }
        else
        {
            return false;
        }

    }

}
