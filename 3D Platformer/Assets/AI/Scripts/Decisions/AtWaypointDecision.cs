using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/AtWaypointDecision", fileName = "At Waypoint Decision")]
public class AtWaypointDecision : Decision
{
    
    
    public override bool Decide(StateController controller)
    {

        float distance = Vector3.Distance(controller.transform.position, controller.currentWaypoint);

        if (distance < (.1f + controller.navMeshAgent.height))
        {
            return true;
        }

        return false;

    }

}
