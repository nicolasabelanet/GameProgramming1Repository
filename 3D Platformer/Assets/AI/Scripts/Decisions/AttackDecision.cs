using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Attack", fileName = "Attack Decision")]
public class AttackDecision : Decision
{

    public override bool Decide(StateController controller)
    {
        float distance = Vector3.Distance(controller.AIEyes.position, controller.chaseTarget.Value.position);

        if (distance < controller.attackRange && controller.canAttack)
        {

            Vector3 direction = (controller.chaseTarget.Value.position - controller.AIEyes.position).normalized;

            if (Vector3.Dot(controller.AIEyes.forward, direction) > .5f)
            {
                controller.canAttack = false;
                controller.timeSinceAttack = 0;
                return true;
            }

        }

        if (!controller.canAttack)
        {
            controller.timeSinceAttack += Time.deltaTime;
        }

        if (controller.timeSinceAttack > controller.attackCooldown)
        {
            controller.canAttack = true;
        }

        return false;
        
    }

}
