using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/State", fileName = "State")]
public class State : ScriptableObject
{
    [SerializeField] private List<Action> enterActions;
    [SerializeField] private List<Action> exitActions;
    [SerializeField] private List<Action> updateActions;

    [SerializeField] private List<Transition> transitions;

    public void EnterState(StateController controller)
    {
        DoEnterActions(controller);
    }

    public void ExitState(StateController controller)
    {
        DoExitActions(controller);
    }

    public void UpdateState(StateController controller)
    {
        DoUpdateActions(controller);

        // Check transitions
        CheckTransitions(controller);
    }

    private void DoEnterActions(StateController controller)
    {
        foreach (Action enterAction in enterActions)
        {
            enterAction.Act(controller);
        }
    }

    private void DoExitActions(StateController controller)
    {
        foreach (Action exitAction in exitActions)
        {
            exitAction.Act(controller);
        }
    }

    private void DoUpdateActions(StateController controller)
    {
        foreach (Action updateAction in updateActions)
        {
            updateAction.Act(controller);
        }
    }

    private void CheckTransitions(StateController controller)
    {
        foreach (Transition transition in transitions)
        {
            // Evaluate the rsult of the transition.
            bool decisionSucceeded = transition.decision.Decide(controller);

            if (decisionSucceeded)
            {
                controller.TransitionToState(transition.nextState);

                // Stop trying to transition
                break;
            }
        }
    }

}


