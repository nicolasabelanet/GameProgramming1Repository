using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CreditsMenuController : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private UIDocument menu;

    private VisualElement root;
    private VisualElement creditsMenu;

    [Header("Properties")]
    [SerializeField] private bool isStatic = false;
    [SerializeField] private bool stowed = true;

    [SerializeField] private float leftStowed = 100.0f;
    [SerializeField] private float startPosition = 60.0f;
    [SerializeField] private float length = 1.0f;

    #region Unity Functions
    
    void Awake() {
        root = menu.rootVisualElement.Q<VisualElement>("root");
        creditsMenu = root.Q<VisualElement>("credits-menu");
    }

    void Start() {
        if (!isStatic) {
            if (stowed) {
                creditsMenu.style.left = Length.Percent(leftStowed);
            }
            else {
                creditsMenu.style.left = Length.Percent(startPosition);
            }
        }
        else {
            creditsMenu.style.left = Length.Percent(startPosition);
        }
        
    }

    #endregion

    private IEnumerator translate(float targetPercent) {
        float counter = 0.0f;

        float distance = Mathf.Abs(creditsMenu.style.left.value.value - targetPercent);

        while (counter < length) {
            counter += Time.deltaTime;
            creditsMenu.style.left = Length.Percent(Mathf.Lerp(creditsMenu.style.left.value.value, targetPercent, (Time.deltaTime / length) * distance));
            yield return null;
        }

        yield return null;
    }

    public void Stow() {

        StopAllCoroutines();

        if (!stowed) {
            StartCoroutine(translate(leftStowed));
        }
        else {
            StartCoroutine(translate(startPosition));
        }

        stowed = !stowed;
        
    }
}
