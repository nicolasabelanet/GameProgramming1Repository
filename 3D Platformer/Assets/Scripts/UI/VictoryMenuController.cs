using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class VictoryMenuController : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private UIDocument gameOverMenu;
    [SerializeField] private GameEvent onQuitButton;

    private VisualElement root;
    private Button quitButton;

    #region Unity Functions

    void Awake() {
        root = gameOverMenu.rootVisualElement.Q<VisualElement>("root");
        quitButton = root.Q<Button>("quit-button");
    }

    void Start()
    {
        root.style.visibility = Visibility.Hidden;
        quitButton.clicked += onQuitButton.Raise;
    }   

    private void OnDestroy() {
        quitButton.clicked -= onQuitButton.Raise;
    }

    #endregion

    public void ShowMenu() {
        root.style.visibility = Visibility.Visible;
    }

}
