using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PauseMenuController : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private UIDocument pauseMenu;
    [SerializeField] private GameEvent onQuitButton;
    [SerializeField] private GameEvent onResumeButton;

    private VisualElement root;
    private Button resumeButton;
    private Button quitButton;

    #region Unity Functions

    void Awake() {
        root = pauseMenu.rootVisualElement.Q<VisualElement>("root");
        resumeButton = root.Q<Button>("resume-button");
        quitButton = root.Q<Button>("quit-button");
    }

    void Start()
    {
        root.style.visibility = Visibility.Hidden;
        resumeButton.clicked += onResumeButton.Raise;
        quitButton.clicked += onQuitButton.Raise;
    }   

    private void OnDestroy() {
        resumeButton.clicked -= onResumeButton.Raise;
        quitButton.clicked -= onQuitButton.Raise;
    }

    #endregion

    public void HideMenu() {
        root.style.visibility = Visibility.Hidden;
    }

    public void ShowMenu() {
        root.style.visibility = Visibility.Visible;
    }

}
