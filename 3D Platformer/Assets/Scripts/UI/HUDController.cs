using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class HUDController : MonoBehaviour
{
    [Header("Heart Properties")]
    [SerializeField] private Sprite heartImage;
    [SerializeField] private int heartSize = 128;
    [SerializeField] private IntReference playerLives;
    private VisualElement root;
    private VisualElement heartContainer;

    #region Unity Functions

    private void Start()
    {
        root = GetComponent<UIDocument>().rootVisualElement;
        heartContainer = root.Q<VisualElement>("heart-container");
        InitializeHearts();
    }

    #endregion

    private void InitializeHearts()
    {
        for (int i = 0; i < playerLives.Value; i++)
        {
            AddHeart();
        }
    }

    // Add a heart to the HUD.
    public void AddHeart()
    {
        Image newHeart = new Image();
        newHeart.sprite = heartImage;
        newHeart.style.paddingTop = 5f;
        newHeart.style.paddingLeft = 0f;
        newHeart.style.paddingRight = 0f;

        newHeart.style.width = heartSize;
        newHeart.style.height = heartSize;

        heartContainer.Add(newHeart);
    }

    // Remove a heart from the HUD.
    public void RemoveHeart()
    {
        if (playerLives.Value > 0)
        {
            heartContainer.RemoveAt(0);
        }
    }

}
