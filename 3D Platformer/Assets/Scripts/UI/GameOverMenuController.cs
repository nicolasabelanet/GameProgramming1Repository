using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class GameOverMenuController : MonoBehaviour
{

    [Header("Components")]
    [SerializeField] private UIDocument gameOverMenu;
    [SerializeField] private GameEvent quitEvent;

    private VisualElement root;
    private Button quitButton;

    #region UnityFunctions

    void Awake() {
        root = gameOverMenu.rootVisualElement.Q<VisualElement>("root");
        quitButton = root.Q<Button>("quit-button");
    }

    void Start()
    {
        root.style.visibility = Visibility.Hidden;
        quitButton.clicked += quitEvent.Raise;
    }   

    private void OnDestroy() {
        quitButton.clicked -= quitEvent.Raise;
    }

    #endregion

    public void ShowMenu() {
        root.style.visibility = Visibility.Visible;
    }

}