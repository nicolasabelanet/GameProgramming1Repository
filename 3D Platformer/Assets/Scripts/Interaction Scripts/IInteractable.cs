/// <summary>
/// An interface indicating that the object can be "interacted" with.
/// </summary>
public interface IInteractable
{

    void Interact(PlayerInteractManager pim, PlayerController pc);

}
