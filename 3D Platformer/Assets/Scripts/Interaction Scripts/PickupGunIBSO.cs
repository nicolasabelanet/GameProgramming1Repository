using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "InteractBehaviors/PickupGun", fileName = "PickupGun")]
public class PickupGunIBSO : InteractBehavior
{

    // Interact function defined in InteractBehavior
    public override void Interact(PlayerInteractManager pim, PlayerController player, GameObject interactable)
    {
        pim.characterCombat.PickupGun();
        Destroy(interactable);
    }

}
