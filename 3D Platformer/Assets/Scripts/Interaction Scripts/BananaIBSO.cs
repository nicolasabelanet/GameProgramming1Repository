using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "InteractBehaviors/Banana", fileName = "Banana")]
public class BananaIBSO : InteractBehavior
{

    // Interact function defined in InteractBehavior
    public override void Interact(PlayerInteractManager pim, PlayerController player, GameObject interactable)
    {
        pim.BulletTime();
        Destroy(interactable);
    }

}
