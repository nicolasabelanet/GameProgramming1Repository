using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Inherits from ScriptableObject rather than Monobehaviour
public abstract class InteractBehavior : ScriptableObject
{
    public abstract void Interact(PlayerInteractManager pim, PlayerController player, GameObject interactable);
}
