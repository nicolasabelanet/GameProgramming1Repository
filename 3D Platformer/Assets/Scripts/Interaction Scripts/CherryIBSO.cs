using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "InteractBehaviors/Cherry", fileName = "Cherry")]
public class CherryIBSO : InteractBehavior
{

    // Interact function defined in InteractBehavior
    public override void Interact(PlayerInteractManager pim, PlayerController player, GameObject interactable)
    {
        player.Heal();
        Destroy(interactable);
    }

}
