using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{

    [Header("Components")]
    [SerializeField] private AudioSource audioSource;

    [Header("Music")]
    [SerializeField] private AudioClip levelMusic;
    [SerializeField] private AudioClip gameOverMusic;
    [SerializeField] private AudioClip victoryMusic;

    [Header("Action Music Setting")]
    [SerializeField] private float waitTime = 10f;

    private float endTime;
    private float counter;
    private Coroutine wait;
    private Coroutine interpolate;

    private void Start()
    {
        PlayLevelMusic();
    }

    public void PlayLevelMusic()
    {
        audioSource.volume = .2f;
        PlaySound(levelMusic);
    }

    public void PlayGameOverMusic()
    {
        audioSource.volume = 1f;
        StopAudioAdjustment();
        PlaySound(gameOverMusic);
    }

    public void PlayVictoryMusic()
    {
        audioSource.volume = 1f;
        StopAudioAdjustment();
        PlaySound(victoryMusic);
    }

    private void StopAudioAdjustment()
    {
        if (wait != null)
        {
            StopCoroutine(wait);
        }

        if (interpolate != null)
        {
            StopCoroutine(interpolate);
        }
    }

    private void PlaySound(AudioClip sound)
    {
        audioSource.clip = sound;
        audioSource.Play();
    }

    private void DestoryPlayer()
    {
        Destroy(this.gameObject);
    }

    public void IncreaseVolume()
    {

        if (audioSource.volume <= .9f)
        {
            audioSource.volume += .1f;
        }

        if (wait != null)
        {
            StopCoroutine(wait);
        }

        wait = StartCoroutine(Wait());

    }

    private IEnumerator Wait()
    {
        if (interpolate != null)
        {
            StopCoroutine(interpolate);
        }

        yield return new WaitForSeconds(waitTime);

        interpolate = StartCoroutine(InterpolateVolume(.2f, .5f));
        yield return null;

    }

    // Interpolate between two floats over a period of time.
    private IEnumerator InterpolateVolume(float end, float time)
    {
        float distance = Mathf.Abs(audioSource.volume - end);

        while (distance > .05f)
        {
            audioSource.volume = Mathf.Lerp(audioSource.volume, end, Time.deltaTime);
            distance = Mathf.Abs(audioSource.volume - end);
            yield return null;
        }

        audioSource.volume = end;
        yield return null;
    }


}
