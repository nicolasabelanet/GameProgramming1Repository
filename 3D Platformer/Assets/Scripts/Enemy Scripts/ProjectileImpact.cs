using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileImpact : MonoBehaviour
{
    [SerializeField] private LayerMask playerMask;

    // Start is called before the first frame update
    void Awake()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 1.5f, playerMask);

        foreach (Collider c in colliders)
        {
            PlayerController player = c.GetComponent<PlayerController>();
            player.Hurt();
        }

    }

}
