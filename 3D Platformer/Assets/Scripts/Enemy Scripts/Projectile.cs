using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private Rigidbody rb;

    [Header("Particles")]
    [SerializeField] private List<ParticleSystem> particles;

    [Header("Instantiables")]
    [SerializeField] private GameObject impact;

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(rb);

        foreach (ParticleSystem p in particles)
        {
            ParticleSystem.EmissionModule e = p.emission;
            e.enabled = false;
        }

        Instantiate(impact, transform.position, Quaternion.identity);

        Destroy(this.gameObject, 5f);
        Destroy(this);

    }

}
