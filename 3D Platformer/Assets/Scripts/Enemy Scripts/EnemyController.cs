using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class EnemyController : MonoBehaviour
{

    [Header("Components")]
    [SerializeField] private Animator animator;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private CapsuleCollider capsuleCollider;
    [SerializeField] private StateController stateController;
    [SerializeField] private EnemyAudio enemyAudio;
    [SerializeField] private AudioSource ambientAudioSource;

    [Header("Enemy Properties")]
    [SerializeField] private int health = 100;

    [Header("Ragdoll Rigidbodies")]
    [SerializeField] private List<Rigidbody> ragdollRigidbodies;

    [Header("Attack Properties")]
    [SerializeField] private float projectileHitTime = .5f;
    [SerializeField] private Transform attackPosition;
    [SerializeField] private TransformReference attackTarget;
    [SerializeField] private RigidbodyReference attackTargetRigidbody;

    [Header("Instantiables")]
    [SerializeField] private GameObject sparks;
    [SerializeField] private GameObject projectile;
    [SerializeField] private GameObject deathExplosion;

    [Header("Events")]
    [SerializeField] private UnityEvent OnAttack;
    [SerializeField] private UnityEvent OnHurt;


    #region Unity Function

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }

    #endregion


    private void UpdateState()
    {
        animator.SetFloat("hSpeed", Mathf.Lerp(animator.GetFloat("hSpeed"), GetHorizontalAgentVelocity().magnitude, Time.deltaTime * 10));
    }

    private Vector3 GetHorizontalAgentVelocity()
    {
        return new Vector3(agent.velocity.x, 0.0f, agent.velocity.z);
    }


    // Damage this enemy.
    public void Damage(int amount, RaycastHit hit)
    {
        OnHurt.Invoke();

        Instantiate(sparks, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));

        health -= amount;

        if (health <= 0)
        {
            Kill(hit.point, hit.normal);
        }
    }


    // Kill this enemy.
    private void Kill(Vector3 hitPosition, Vector3 hitNormal)
    {

        Instantiate(deathExplosion, hitPosition, Quaternion.identity, ragdollRigidbodies[0].transform);

        Destroy(capsuleCollider);
        Destroy(agent);
        Destroy(animator);
        Destroy(stateController);
        Destroy(enemyAudio);
        Destroy(ambientAudioSource);

        foreach (Rigidbody rb in ragdollRigidbodies)
        {
            rb.isKinematic = false;
        }

        foreach (Rigidbody rb in ragdollRigidbodies)
        {
            rb.AddForce(-hitNormal * 20, ForceMode.VelocityChange);
        }

        Destroy(this);

    }


    // Attack the attack target.
    public void Attack()
    {
        Vector3 projectedPosition = attackTarget.Value.position + (attackTargetRigidbody.Value.velocity * (projectileHitTime / Time.timeScale));
        Vector3 attackVector = (projectedPosition - attackPosition.position) * (Time.timeScale / projectileHitTime);

        GameObject proj = Instantiate(projectile, attackPosition.position, Quaternion.FromToRotation(Vector3.forward, attackVector.normalized));

        proj.GetComponent<Rigidbody>().AddForce(attackVector, ForceMode.VelocityChange);
        OnAttack.Invoke();

    }

}
