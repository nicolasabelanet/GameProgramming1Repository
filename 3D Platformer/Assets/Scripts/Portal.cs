using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    [SerializeField] private string nextLevelName;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (nextLevelName != null && nextLevelName != "")
            {
                GameManager.Instance.LoadScene(nextLevelName);
            }
            else
            {
                GameManager.Instance.Victory();
            }
        }
    }
}
