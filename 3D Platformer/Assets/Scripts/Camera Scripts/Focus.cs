using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;


public class Focus : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private Volume volume;

    [Header("Focus Properties")]
    [SerializeField] public TransformReference focusTarget;
    [SerializeField] private DepthOfField dof;

    private VolumeProfile volumeProfile;

    private void Awake()
    {
        DepthOfField tmp;
        if (volume.profile.TryGet<DepthOfField>(out tmp))
        {
            dof = tmp;
        }
    }

    // Update is called once per frame
    void Update()
    {
        dof.focusDistance.value = Vector3.Distance(focusTarget.Value.position, transform.position);
    }

}
