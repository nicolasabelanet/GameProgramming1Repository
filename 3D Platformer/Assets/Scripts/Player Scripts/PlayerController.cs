using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.Animations.Rigging;
using Cinemachine;

public class PlayerController : MonoBehaviour
{

    // Player Input
    private Vector2 movementInput;
    private Vector3 cameraAdjustedInputDirection;

    [Header("Components")]
    [SerializeField] private CinemachineVirtualCamera aimCamera;
    [SerializeField] private CharacterMovement characterMovement;
    [SerializeField] private CharacterCombat characterCombat;
    [SerializeField] private PlayerInteractManager pim;

    [Header("Camera")]
    [SerializeField] private TransformReference cameraTransform;
    [SerializeField] private Transform cameraOrientation;

    // The player input actions asset.
    private PlayerInputActions playerInputActions;

    [Header("State Information")]
    [SerializeField] private bool hInput = false;
    [SerializeField] private bool aimInput = false;

    [Header("Events")]
    [SerializeField] private GameEvent OnHurt;
    [SerializeField] private GameEvent OnHeal;
    [SerializeField] private GameEvent OnKill;
    [SerializeField] private GameEvent onPauseAction;
    [SerializeField] private GameEvent onResumeAction;


    // Public properties
    public bool HInput { get; private set; }

    #region Events

    public void SwitchToPlayerMap()
    {
        SwitchActionMap("Player");
    }

    public void SwitchToUIMap()
    {
        SwitchActionMap("UI");
    }

    #endregion


    #region Unity Function

    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
        SubscribeInputActions();
        SwitchActionMap("Player");
    }

    private void OnDestroy()
    {
        UnsubscribeInputActions();
    }

    // Update is called once per frame
    void Update()
    {
        characterCombat.Aim(aimInput);
        // Call our rotation for the camera orientation object and player
        RotateCameraAndCharacter();
        // Calculate the new relative input.
        CalculateCameraRelativeInput();
        UpdateState();
    }

    private void FixedUpdate()
    {
        characterMovement.Move(cameraAdjustedInputDirection);
    }

    #endregion

    private void UpdateState()
    {
        HInput = hInput;
    }

    private void RotateCameraAndCharacter()
    {

        Vector3 targetDir = cameraTransform.Value.forward;
        targetDir.y = 0;

        cameraOrientation.forward = targetDir.normalized;

        // Now rotate the character.
        if (aimInput)
        {
            characterMovement.RotateCharacter(cameraOrientation.forward);
        }
        else
        {
            characterMovement.RotateCharacter(cameraAdjustedInputDirection);
        }

    }

    private void CalculateCameraRelativeInput() {
        cameraAdjustedInputDirection = cameraOrientation.forward * movementInput.y + cameraOrientation.right * movementInput.x;

        // Possibly normalize if our vector is too big
        if (cameraAdjustedInputDirection.sqrMagnitude > 1) {
            cameraAdjustedInputDirection = cameraAdjustedInputDirection.normalized;
        }

        if (cameraAdjustedInputDirection != Vector3.zero)
        {
            hInput = true;
        }
        else
        {
            hInput = false;
        }
    }

    #region Input Callbacks

    private void MoveActionPerformed(InputAction.CallbackContext context) {
        // Get the user input.
        movementInput = context.ReadValue<Vector2>();

        // Get the relative input direction.
        CalculateCameraRelativeInput();

        // Now actually move the character using the movement component.
        characterMovement.Move(cameraAdjustedInputDirection);
    }

    private void DeactivateAim(InputAction.CallbackContext context)
    {
        aimInput = false;
        aimCamera.gameObject.SetActive(false);
    }

    private void ActivateAim(InputAction.CallbackContext context)
    {
        aimInput = true;
        aimCamera.gameObject.SetActive(true);
    }

    private void JumpActionPerformed(InputAction.CallbackContext context)
    {
        characterMovement.Jump();
    }

    private void JumpActionCanceled(InputAction.CallbackContext context)
    {
        characterMovement.JumpCancel();
    }

    private void RunActionPerformed(InputAction.CallbackContext context)
    {
        characterMovement.Run();
    }

    private void RunActionCancelled(InputAction.CallbackContext context)
    {
        characterMovement.Walk();
    }

    private void InteractionPerformed(InputAction.CallbackContext context)
    {
        pim.Interact();
    }

    private void FireActionPerformed(InputAction.CallbackContext context)
    {
        characterCombat.Fire();
    }

    #endregion

    private void SubscribeInputActions() {
        // Subscribe to movement actions.
        playerInputActions.Player.Move.started += MoveActionPerformed;
        playerInputActions.Player.Move.performed += MoveActionPerformed;
        playerInputActions.Player.Move.canceled += MoveActionPerformed;

        // Subscribe to jump actions
        playerInputActions.Player.Jump.started += JumpActionPerformed;
        playerInputActions.Player.Jump.canceled += JumpActionCanceled;

        // Subscribe to run actions
        playerInputActions.Player.Run.started += RunActionPerformed;
        playerInputActions.Player.Run.canceled += RunActionCancelled;

        // Subscribe to camera actions
        playerInputActions.Player.Aim.started += ActivateAim;
        playerInputActions.Player.Aim.performed += ActivateAim;
        playerInputActions.Player.Aim.canceled += DeactivateAim;

        // Subscribe to pause actions
        playerInputActions.Player.Pause.started += PauseGame;
        playerInputActions.UI.Resume.started += ResumeGame;

        // Subscribe to interaction actions
        playerInputActions.Player.Interact.started += InteractionPerformed;

        // Subscribe to fire actions
        playerInputActions.Player.Fire.started += FireActionPerformed;
    }

    private void UnsubscribeInputActions() {
        // Unubscribe to movement actions.
        playerInputActions.Player.Move.started -= MoveActionPerformed;
        playerInputActions.Player.Move.performed -= MoveActionPerformed;
        playerInputActions.Player.Move.canceled -= MoveActionPerformed;

        // Unubscribe to jump actions
        playerInputActions.Player.Jump.started -= JumpActionPerformed;
        playerInputActions.Player.Jump.canceled -= JumpActionCanceled;

        // Unubscribe to run actions
        playerInputActions.Player.Run.started -= RunActionPerformed;
        playerInputActions.Player.Run.canceled -= RunActionCancelled;

        // Subscribe to camera actions
        playerInputActions.Player.Aim.started -= ActivateAim;
        playerInputActions.Player.Aim.performed -= ActivateAim;
        playerInputActions.Player.Aim.canceled -= DeactivateAim;

        // Unsubscribe to pause actions
        playerInputActions.Player.Pause.started -= PauseGame;
        playerInputActions.UI.Resume.started -= ResumeGame;

        // Unsubscribe to interaction actions
        playerInputActions.Player.Interact.started -= InteractionPerformed;

        // Unsubscribe to fire actions
        playerInputActions.Player.Fire.started -= FireActionPerformed;
    }

    // Switch to a given action map.
    private void SwitchActionMap(string mapName) {
        switch(mapName) {
            case "Player":
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                playerInputActions.UI.Disable();
                playerInputActions.Player.Enable();
                break;
            case "UI":
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
                playerInputActions.Player.Disable();
                playerInputActions.UI.Enable();
                break;
        }
    }

    public void PauseGame(InputAction.CallbackContext context)
    {
        onPauseAction.Raise();
    }

    public void ResumeGame(InputAction.CallbackContext context)
    {
        onResumeAction.Raise();
    }

    public void Hurt()
    {
        OnHurt.Raise();
    }

    public void Heal()
    {
        OnHeal.Raise();
    }

}
