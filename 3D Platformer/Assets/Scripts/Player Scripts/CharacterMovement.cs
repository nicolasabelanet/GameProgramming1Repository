using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class CharacterMovement : BaseMovement
{

    [Header("Components")]
    [SerializeField] private Rigidbody characterRigidbody;
    [SerializeField] private CapsuleCollider capsule;
    [SerializeField] private Transform characterModel;

    [Header("Ground Movement")]
    [Tooltip("The speed that this character accelerates at")]
    [SerializeField] private float maxWalkSpeed = 3.5f;
    [SerializeField] private float maxRunSpeed = 5f;
    [SerializeField] private float moveAcceleration = 60;
    [SerializeField] private float rotationSpeed = 10.0f;
    [SerializeField] private float sprintTransitionTime = .3f;

    private float maxSpeed = 7f;

    [Header("Ground Snap Properties")]
    [SerializeField] private float snapDistance = .25f;
    [SerializeField] private float snapForceMultiplier = 2f;

    [Header("Ground Check")]
    [Tooltip("Maximum distance below player to be considered grounded.")]
    [SerializeField] private float groundCheckDistance = 0.1f;
    [SerializeField] private LayerMask environmentMask;

    [Header("Air Movement")]
    [SerializeField] private float fallForce = 2.0f;
    [SerializeField] private float maxVerticalMoveSpeed = 25.0f;
    [SerializeField] private float jumpForce = 7.0f;
    [SerializeField] private float hardLandingVeloicty = -6.0f;
    [SerializeField] private float artificalGravityVeloctyThreshold = 2.0f;

    // Input variables
    private Vector3 cameraAdjustedInputDirection;
    private Vector3 direction;

    // Max Speed Interpolation Coroutine
    private Coroutine maxSpeedCoroutine;

    [Header("State Information")]
    [SerializeField] private bool isGrounded = true;
    [SerializeField] private bool isOnSlope = false;
    [SerializeField] private bool isRunning = false;
    [SerializeField] private bool hardLanding = false;
    [SerializeField] private bool isRolling = false;

    private bool lockJump = false;
    private int stepsSinceLastGrounded = 0;
    private int stepsSinceLastJumped = 0;

    // Raycast hits
    private RaycastHit slopeHit;

    [Header("Events")]
    [SerializeField] private GameEvent OnJump;

    // Public parameters
    public bool IsGrounded { get; private set; }
    public bool HardLanding { get; private set; }
    public bool IsRolling { get; private set; }
    public bool IsRunning { get; private set; }

    // Relative to player object
    public Vector3 PlayerVelocity { get; private set; }
    public Vector3 PlayerHorizontalVelocity { get; private set; }

    // Relative to mesh object
    public float ModelForwardVelocity { get; private set; }
    public float ModelRightVelocity { get; private set; }
    public int StepsSinceLastGrounded { get; private set; }


    #region Unity Functions

    protected override void Awake()
    {
        maxSpeed = maxWalkSpeed;
    }


    override protected void FixedUpdate()
    {
        MoveCharacter();
        CheckGrounded();
        CheckSlope();
        CheckSnap();
        CheckFalling();
        LimitVelocity();
        ApplyRotation();
        CheckHardLanding();
        CheckRolling();
        UpdateState();
    }

    #endregion


    // Check to see whether or not the player
    // should be snapped to the ground.
    private void CheckSnap()
    {
        stepsSinceLastJumped += 1;
        stepsSinceLastGrounded += 1;
        if (isGrounded || SnapToGround())
        {
            stepsSinceLastGrounded = 0;
        }
    }


    // Check to see if the player is going to
    // roll when landing next.
    private void CheckRolling()
    {
        if (hardLanding && isGrounded)
        {
            hardLanding = false;
            StartCoroutine(Rolling());
        }
    }


    // A coroutine to update state
    // while rolling.
    private IEnumerator Rolling()
    {
        isRolling = true;
        yield return new WaitForSeconds(1.2f);
        isRolling = false;
    }


    // Check to see if the player is going
    // to have a hard landing.
    private void CheckHardLanding()
    {
        if (!isGrounded && characterRigidbody.velocity.y < hardLandingVeloicty && !hardLanding)
        {
            hardLanding = true;
        }
    }


    private bool SnapToGround()
    {

        if (!isOnSlope)
        {
            return false;
        }

        if (stepsSinceLastGrounded > 1 || stepsSinceLastJumped <= 2)
        {
            return false;
        }

        if (slopeHit.distance < snapDistance)
        {
            characterRigidbody.AddForce(-slopeHit.normal * snapForceMultiplier, ForceMode.Impulse);
        }

        return true;

    }


    private void CheckFalling()
    {
        if (characterRigidbody.velocity.y < artificalGravityVeloctyThreshold)
        {
            characterRigidbody.AddForce(Vector3.down * fallForce * characterRigidbody.mass, ForceMode.Force);
        }
    }


    // Check to see if the player is grounded.
    private void CheckGrounded()
    {
        // Use an overlappign sphere check to see if we are grounded.
        Vector3 origin = transform.position + (Vector3.up * (capsule.radius - groundCheckDistance));

        // Query physics engine for collisions
        Collider[] overlappedCollider = Physics.OverlapSphere(origin, capsule.radius * .95f, environmentMask, QueryTriggerInteraction.Ignore);

        // Determine if we're grounded
        isGrounded = (overlappedCollider.Length > 0);

    }


    private void CheckSlope()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out slopeHit, 1, environmentMask))
        {
            Debug.DrawLine(transform.position, transform.position + slopeHit.normal, Color.red);

            if (slopeHit.normal != Vector3.up)
            {
                isOnSlope = true;
            }

        }
        else
        {
            isOnSlope = false;
        }

    }

        
    // Move the character.
    private void MoveCharacter() {

        // Add force if there is input.
        if (cameraAdjustedInputDirection != Vector3.zero) {
            Vector3 force = cameraAdjustedInputDirection * moveAcceleration;
            characterRigidbody.AddForce(force, ForceMode.Acceleration);

        }

    }


    private Vector3 GetHorizontalRBVelocity()
    {
        return new Vector3(characterRigidbody.velocity.x, 0.0f, characterRigidbody.velocity.z);
    }


    // Ensures that the player does not exceed the max velocity.
    private void LimitVelocity()
    {
        // Get our horizontal velocity
        Vector3 currentVelocity = GetHorizontalRBVelocity();

        // Get our max velocity
        float maxAllowedVelocity = GetMaxAllowedVelocity();

        
        if (currentVelocity.sqrMagnitude > (maxAllowedVelocity * maxAllowedVelocity))
        {
            // Use an impulse force to counteract the speed of this player
            Vector3 counteractDirection = currentVelocity.normalized * -1.0f;
            float counteractAmount = currentVelocity.magnitude - maxAllowedVelocity;

            characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
        }

        // Now check for falling/jumping
        if (!isGrounded)
        {
            if (Mathf.Abs(characterRigidbody.velocity.y) > maxVerticalMoveSpeed)
            {
                // Use an impulse to counteract the speed.
                Vector3 counteractDirection = Vector3.up * Mathf.Sign(characterRigidbody.velocity.y) * -1f;

                float counteractAmount = Mathf.Abs(characterRigidbody.velocity.y) - maxVerticalMoveSpeed;

                characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
            }
        }

    }


    private float GetMaxAllowedVelocity()
    {
        return maxSpeed;
    }


    override public void Move(Vector3 moveDirection) {
        cameraAdjustedInputDirection = moveDirection;
    }


    override public void Jump()
    {

        if (isGrounded && !lockJump)
        {
            stepsSinceLastJumped = 0;
            OnJump.Raise();
            ApplyJump();
        }
    }


    override public void Run()
    {
        isRunning = true;

        // Check to see if the max speed
        // is already being interpolated.
        if (maxSpeedCoroutine != null)
        {
            StopCoroutine(maxSpeedCoroutine);
        }

        // Interpolate to the maxRunSpeed.
        maxSpeedCoroutine = StartCoroutine(interpolateMaxSpeed(maxSpeed, maxRunSpeed, sprintTransitionTime));
    }


    override public void Walk()
    {
        isRunning = false;

        // Check to see if the max speed
        // is already being interpolated.
        if (maxSpeedCoroutine != null)
        {
            StopCoroutine(maxSpeedCoroutine);
        }

        // Interpolate to the maxWalkSpeed.
        maxSpeedCoroutine = StartCoroutine(interpolateMaxSpeed(maxSpeed, maxWalkSpeed, sprintTransitionTime));
    }


    // Interpolate between two floats over a period of time.
    private IEnumerator interpolateMaxSpeed(float start, float end, float time)
    {
        float counter = 0;

        while (counter < time)
        {
            maxSpeed = Mathf.Lerp(start, end, counter / time);
            counter += Time.deltaTime;
            yield return null;
        }

        maxSpeed = end;
        yield return null;
    }


    // Prevent the player from jumping.
    public void LockJump()
    {
        lockJump = true;
    }


    // Allow the player to jump.
    public void UnlockJump()
    {
        lockJump = false;
    }


    // Add the jumping force.
    private void ApplyJump()
    {
        characterRigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }


    // Apply force in the opposite direction.
    override public void JumpCancel()
    {
        // Cancel the jump if jumping.
        if (characterRigidbody.velocity.y > 0)
        {
            characterRigidbody.AddForce(Vector3.down * characterRigidbody.velocity.y * characterRigidbody.mass *.5f, ForceMode.Impulse);
        }

    }


    // Rotate our character.
    override public void RotateCharacter(Vector3 rotation)
    {
        direction = rotation;
    }
        

    // Rotate our character.
    private void ApplyRotation()
    {
        characterModel.forward = Vector3.Slerp(characterModel.forward, direction.normalized, Time.fixedDeltaTime * rotationSpeed);
    }


    // Update public parameters.
    private void UpdateState()
    {
        IsGrounded = isGrounded;
        IsRunning = isRunning;
        HardLanding = hardLanding;
        IsRolling = isRolling;

        PlayerVelocity = characterRigidbody.velocity;
        PlayerHorizontalVelocity = GetHorizontalRBVelocity();

        ModelForwardVelocity = Vector3.Dot(characterModel.forward, PlayerHorizontalVelocity);
        ModelRightVelocity = Vector3.Dot(characterModel.right, PlayerHorizontalVelocity);

        StepsSinceLastGrounded = stepsSinceLastGrounded;
    }


}
