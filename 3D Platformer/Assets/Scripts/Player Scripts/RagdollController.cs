using System.Collections;
using System.Collections.Generic;
using UnityEngine.Animations.Rigging;
using UnityEngine;

public class RagdollController : MonoBehaviour
{
    [Header("On Ragdoll Destroy Components")]
    [SerializeField] private PlayerController playerController;
    [SerializeField] private CharacterCombat characterCombat;
    [SerializeField] private CharacterMovement characterMovement;
    [SerializeField] private PlayerInteractManager playerInteractManager;
    [SerializeField] private CapsuleCollider capsuleCollider;
    [SerializeField] private Rigidbody playerRigidbody;
    [SerializeField] private CharacterAnimation characterAnimation;
    [SerializeField] private CharacterAudio characterAudio;
    [SerializeField] private BoxCollider boxCollider;
    [SerializeField] private RigBuilder rigBuilder;
    [SerializeField] private Animator animator;

    [Header("Ragdoll Components")]
    [SerializeField] private List<Collider> ragdollColliders;
    [SerializeField] private List<Rigidbody> ragdollRigidbodies;

    public void EnableRagdoll()
    {
        // Destroy all player components.
        Destroy(playerController);
        Destroy(characterCombat);
        Destroy(characterMovement);
        Destroy(playerInteractManager);
        Destroy(capsuleCollider);
        Destroy(characterAnimation);
        Destroy(characterAudio);
        Destroy(boxCollider);
        Destroy(playerRigidbody);
        Destroy(rigBuilder);
        Destroy(animator);


        // Enable ragdoll.
        foreach (Rigidbody r in ragdollRigidbodies)
        {
            r.isKinematic = false;
        }

        foreach(Collider c in ragdollColliders)
        {
            c.enabled = true;
        }

    }
}
