using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAudio : MonoBehaviour
{

    [Header("Components")]
    [SerializeField] private AudioSource source;
    [SerializeField] private CharacterCombat combat;
    [SerializeField] private CharacterMovement movement;

    [Header("Audio Settings")]
    [SerializeField] private float pitchMin;
    [SerializeField] private float pitchMax;

    [Header("Sounds")]
    [SerializeField] private MaterialAudioCollection footstepCollection;
    [SerializeField] private AudioClip fireSound;
    [SerializeField] private AudioClip pickupSound;
    [SerializeField] private AudioClip hurtSound;
    [SerializeField] private AudioClip healSound;

    [Header("Footstep Properties")]
    [SerializeField] private float aimingWalkFootstepFrequency = .4f;
    [SerializeField] private float aimingRunFootstepFrequency = .3f;
    [SerializeField] private bool isLooping = false;


    // State information
    private bool wasUngrounded = false;
    private int ungroundedSteps = 0;

    private Coroutine AimingFootstepLoop;


    #region Unity Functions

    private void Update()
    {
        CheckLand();
        CheckAiming();
    }

    #endregion


    public void PlayPickupSound()
    {
        PlaySoundOnce(pickupSound);
    }


    public void PlayHealSound()
    {
        PlaySoundOnce(healSound);
    }


    public void PlayHurtSound()
    {
        PlaySoundOnce(hurtSound);
    }


    public void PlayFootstep()
    {

        AudioClip footstep;

        if (movement.IsRunning)
        {
            footstep = footstepCollection.GetRunSound();
        }
        else
        {
            footstep = footstepCollection.GetWalkSound();
        }

        PlaySound(footstep);
        
    }


    public void PlayFireSound()
    {
        PlaySoundOnce(fireSound);
    }


    public void PlayJumpSound()
    {
        AudioClip jumpSound = footstepCollection.GetJumpSound();
        PlaySound(jumpSound);
    }


    public void PlayLandSound()
    {
        AudioClip landSound = footstepCollection.GetLandSound();
        PlaySound(landSound);
    }


    private void PlaySound(AudioClip sound)
    {
        source.clip = sound;
        source.pitch = Random.Range(pitchMin, pitchMax);
        source.Play();
    }


    private void PlaySoundOnce(AudioClip sound)
    {
        source.pitch = Random.Range(pitchMin, pitchMax);
        source.PlayOneShot(sound);
    }


    private void CheckAiming() 
    {
        if (combat.IsAiming && !isLooping)
        {
            StartCoroutine(PlayFootstepLoop()); 
        }
    }


    private IEnumerator PlayFootstepLoop()
    {
        isLooping = true;

        while (combat.IsAiming)
        {
            if (movement.PlayerHorizontalVelocity.magnitude > .1)
            {
                if (movement.IsRunning)
                {
                    yield return new WaitForSeconds(aimingRunFootstepFrequency);
                }
                else
                {
                    yield return new WaitForSeconds(aimingWalkFootstepFrequency);
                }

                PlayFootstep();
            }

            yield return null;

        }

        isLooping = false;

    }


    private void CheckLand()
    {
        if (wasUngrounded && ungroundedSteps > 2 && movement.IsGrounded)
        {
            PlayLandSound();
        }

        wasUngrounded = !movement.IsGrounded;
        ungroundedSteps = movement.StepsSinceLastGrounded;
    }


}
