using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPickup : MonoBehaviour
{

    [Header("Components")]
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Hover hover;
    [SerializeField] private AudioSource audioSource;

    [Header("Pickup Properties")]
    [SerializeField] private TransformReference cameraReference;

    [Header("State Information")]
    [SerializeField] private bool landed = false;

    // Start is called before the first frame update
    void Start()
    {
        if (landed)
        {
            StartCoroutine(FacePlayer());
        }
    }

    private void OnCollisionEnter(Collision other)
    {

        if (!landed)
        {
            if (!other.gameObject.CompareTag("Player")) {
                landed = true;
                audioSource.Play();
                transform.localScale = transform.localScale * 2;
            
                Destroy(rb);
                transform.rotation = Quaternion.identity;
                hover.SetHover(transform.position);
                StartCoroutine(FacePlayer());
            }

        }
    }


    private IEnumerator FacePlayer()
    {
        while (true)
        {
            Vector3 direction = cameraReference.Value.position - transform.position;
            direction.y = 0;

            transform.right = direction;
            yield return new WaitForFixedUpdate();
        }
    }

}
