using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using UnityEngine.Events;

public class CharacterCombat : MonoBehaviour
{
    // The center of the screen.
    private Vector2 screenCenterPoint = new Vector2(Screen.width / 2, Screen.height / 2);

    [Header("Components")]
    [SerializeField] private CharacterMovement movement;
    [SerializeField] private Rig rig;

    [Header("Gun Properties")]
    [SerializeField] private MeshRenderer gunRenderer;
    [SerializeField] private Transform firePoint;
    [SerializeField] private Transform gun;
    [SerializeField] private LayerMask nonPlayerMask;
    [SerializeField] private float projectileSpeed = 800f;
    [SerializeField] private float recoilAngle = 15f;

    [Header("Aim Target Properties")]
    [SerializeField] private TransformVariable aimTarget;
    [SerializeField] private float aimTargetInterpolationSpeed = 20.0f;

    [Header("Aim Properties")]
    [SerializeField] private float aimSpeed = 10.0f;
    [SerializeField] private float aimSnapThreshold = .1f;
    [SerializeField] private float unAimSpeed = 5.0f;
    [SerializeField] private float unAimSnapThreshold = .01f;


    [Header("State Information")]
    [SerializeField] private bool isAiming = false;
    [SerializeField] private bool canShoot = false;

    [Header("Instantiables")]
    [SerializeField] private GameObject muzzleFlash;
    [SerializeField] private GameObject impact;
    [SerializeField] private GameObject gunPickup;

    [Header("Events")]
    [SerializeField] private GameEvent OnFire;
    [SerializeField] private GameEvent OnGunPickup;
    
    // Public Properties
    public bool IsAiming { get; private set; }

    #region Unity Functions

    private void Update() { 
        UpdateAim();
        UpdateState();
    }

    #endregion


    // Fire the weapon if able.
    public void Fire()
    {
       if (isAiming && canShoot)
       {
            ApplyFire();
       }
        
    }


    // Fire the gun.
    private void ApplyFire()
    {

        OnFire.Raise();

        // Instantiate the muzzle flash.
        GameObject mf = Instantiate(muzzleFlash, firePoint.position, Quaternion.FromToRotation(Vector3.up, firePoint.forward));

        // Apply recoil to the character.
        Recoil();

        // The ray pointing in the center of the screen.
        Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
        RaycastHit hit;

        // Raycast to see if we hit anything.
        if (Physics.Raycast(ray, out hit, 999f, nonPlayerMask))
        {
            Impact(transform.position, hit);
        }

        // Spawn the pickup in.
        SpawnGunPickup(mf.transform);

        // Disable gun.
        gunRenderer.enabled = false;
        canShoot = false;
    }


    // Pickup the gun.
    public void PickupGun()
    {
        OnGunPickup.Raise();
        gunRenderer.enabled = true;
        canShoot = true;
    }


    //  Create the gun pickup.
    private void SpawnGunPickup(Transform muzzleFlash)
    {
        // Insantiate the gun pickup.
        GameObject pickup = Instantiate(gunPickup, gun.position, gun.rotation);

        // Parent the muzzle flash to the pickup.
        muzzleFlash.parent = pickup.transform;

        // Get the rigidbody
        Rigidbody rb = pickup.GetComponent<Rigidbody>();

        // Get a random force vector;
        Vector3 random = Random.onUnitSphere;
        Vector3 force = (firePoint.up * 5) + random * 5;

        // Add random force and torque.
        rb.AddForce(force, ForceMode.VelocityChange);
        rb.AddRelativeTorque(force, ForceMode.VelocityChange);
    }


    // Apply recoil to the gun;
    public void Recoil()
    {
        float distance = Vector3.Distance(firePoint.position, aimTarget.Value.position);
        float amount = distance * Mathf.Tan(recoilAngle * Mathf.Deg2Rad);
        aimTarget.Value.position += firePoint.up * amount;
    }


    // Calculate and respond to gun impact.
    private void Impact(Vector3 source, RaycastHit hit)
    {
        // Calculate how long it would take the bullet
        // to reach the hit point.
        float distance = Vector3.Distance(source, hit.point);
        float waitTime = distance / projectileSpeed;

        Debug.Log(waitTime / Time.timeScale);

        StartCoroutine(ImpactSequence(waitTime, hit));
        
    }

    private IEnumerator ImpactSequence(float seconds, RaycastHit hit)
    {
        // Wait for the bullet to reach its target
        yield return new WaitForSeconds(seconds);

        // Attempt to damage an enemy.
        TryDamageEnemy(hit);
   
        Debug.Log(hit.collider);

        // Instantiate an impact effect at the hit point.
        Instantiate(impact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
    }


    // See if what was hit is an enemy.
    private void TryDamageEnemy(RaycastHit hit)
    {
        if (hit.collider.CompareTag("Arm"))
        {
            DamageEnemy(20, hit);
        }

        else if(hit.collider.CompareTag("Leg"))
        {
            DamageEnemy(20, hit);
        }

        else if(hit.collider.CompareTag("Body"))
        {
            DamageEnemy(50, hit);
        }

        else if (hit.collider.CompareTag("Head"))
        {
           DamageEnemy(100, hit);
        }

    }


    // Damage an enemy.
    private void DamageEnemy(int amount, RaycastHit hit)
    {

        Transform current = hit.collider.transform.parent;

        // Travel up the enemy object until
        // it reaches the root transform.
        while (current.parent != null)
        {
            current = current.parent;
        }


        // This must be the enemy.
        EnemyController enemyController = current.GetComponent<EnemyController>();

        // Ensure no errors.
        if (enemyController != null)
        {
            enemyController.Damage(amount, hit);
        }
        else
        {
            Debug.LogError("This should not be null.");
        }
    }

    // Update public state.
    private void UpdateState()
    {
        IsAiming = isAiming;
    }

    public void Aim(bool aiming)
    {
        if (movement.IsGrounded && !movement.IsRolling && canShoot && aiming)
        {
            isAiming = true;
        }
        else
        {
            isAiming = false;
        }
    }


    // Update the aim.
    private void UpdateAim()
    {

        if (isAiming)
        {
            if (Mathf.Abs(rig.weight - 1) < aimSnapThreshold)
            {
                rig.weight = 1;
            }
            else
            {
                rig.weight = Mathf.Lerp(rig.weight, 1.0f, Time.deltaTime * aimSpeed);
            }
        }
        
        else
        {
            if (rig.weight < unAimSnapThreshold)
            {
                rig.weight = 0;
            }
            else
            {
                rig.weight = Mathf.Lerp(rig.weight, 0, Time.deltaTime * unAimSpeed);
            }
        }

        UpdateTargetPosition();

    }


    // Update the aim target.
    private void UpdateTargetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
        Vector3 targetPosition;
        if (Physics.Raycast(ray, out RaycastHit hit, 999f, nonPlayerMask))
        {
            targetPosition = hit.point;
        }
        else
        {
            targetPosition = transform.position + ray.direction * 100;
        }

        aimTarget.Value.position = Vector3.Lerp(aimTarget.Value.position, targetPosition, Time.deltaTime * aimTargetInterpolationSpeed);
    }

}
