using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private PlayerController player;
    [SerializeField] private CharacterMovement movement;
    [SerializeField] private CharacterCombat combat;
    [SerializeField] private Animator animator;

    [Header("Animation Properties")]
    [SerializeField] private float referenceMovementSpeed = 3.609628f;
    [SerializeField] private float referenceRollingSpeed = 7.0f;

    // State information
    private float maxFallSpeed;


    #region Unity Function

    private void Update()
    {
        UpdateAnimation();
    }

    #endregion


    private void UpdateAnimation()
    {
        // Movement
        Vector3 hVelocity = movement.PlayerHorizontalVelocity;
        Vector3 velocity = movement.PlayerVelocity;
        float vVelocity = velocity.y;

        animator.SetFloat("forwardVelocity", Mathf.Lerp(animator.GetFloat("forwardVelocity"), movement.ModelForwardVelocity, Time.deltaTime * 10));
        animator.SetFloat("rightVelocity", Mathf.Lerp(animator.GetFloat("rightVelocity"), movement.ModelRightVelocity, Time.deltaTime * 10));
        animator.SetBool("hInput", player.HInput);
        animator.SetFloat("hSpeed", Mathf.Lerp(animator.GetFloat("hSpeed"), hVelocity.magnitude, Time.deltaTime * 10));
        animator.SetFloat("vSpeed", Mathf.Abs(vVelocity));
        animator.SetFloat("vVelocity", vVelocity);

        if (movement.HardLanding)
        {
            animator.SetTrigger("HardLanding");
        }

        if (movement.StepsSinceLastGrounded > 2)
        {
            animator.SetBool("isGrounded", false);
        }
        else
        {
            animator.SetBool("isGrounded", true);
        }

        animator.SetFloat("jumpSpeedMultiplier", CalculateJumpSpeedMultiplier(vVelocity, movement.IsGrounded));
        animator.SetFloat("movementSpeedMultiplier", CalculateRunningSpeedMultiplier(hVelocity));

        if (!movement.IsGrounded)
        {
            animator.SetFloat("maxFallSpeed", CheckMaxFallSpeed(vVelocity, movement.IsGrounded));
            animator.SetFloat("rollingSpeedMultiplier", CalculateRollingSpeedMultiplier(velocity));
        }

        animator.SetBool("isAiming", combat.IsAiming);
    }


    public void TriggerJump()
    {
        animator.SetTrigger("Jump");
    }


    // Caculate the multiplier necessary to match
    // the players movemenement animation to their
    // actually velocity.
    private float CalculateRunningSpeedMultiplier(Vector3 hVelocity)
    { 
        float multiplier = hVelocity.magnitude / referenceMovementSpeed;

        multiplier = Mathf.Max(.5f, multiplier);

        return multiplier;
    }


    // Caculate the multiplier necessary to match
    // the players jump animation to their
    // actually velocity.
    private float CalculateJumpSpeedMultiplier(float vVelocity, bool isGrounded)
    {

        if (Mathf.Abs(vVelocity) > 1 && !isGrounded)
        {
            return 1 / Mathf.Abs(vVelocity);
        }

        return 1f;

    }

    // Caculate the multiplier necessary to match
    // the players rolling animation to their
    // actually velocity.
    private float CalculateRollingSpeedMultiplier(Vector3 velocity)
    {
        float speed = velocity.magnitude;

        float multiplier = speed / referenceRollingSpeed;

        return Mathf.Clamp(multiplier, .9f, 1.5f); ;
    }


    // Calculate the maximum the speed the player has fallen
    // while being in the air.
    private float CheckMaxFallSpeed(float vVelocity, bool isGrounded)
    {
        if (isGrounded || vVelocity > 0f)
        {
            maxFallSpeed = 0.0f;
        }

        return Mathf.Max(maxFallSpeed, -vVelocity);
    }


}
