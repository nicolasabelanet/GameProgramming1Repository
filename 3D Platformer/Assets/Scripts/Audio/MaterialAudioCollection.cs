using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AudioCollections/MaterialCollection", fileName = "MaterialAudioCollection")]
public class MaterialAudioCollection : ScriptableObject
{

    [SerializeField] private List<AudioClip> walkSounds;
    [SerializeField] private List<AudioClip> runSounds;
    [SerializeField] private List<AudioClip> landSounds;
    [SerializeField] private List<AudioClip> jumpSounds;


    public AudioClip GetWalkSound()
    {
        return walkSounds[Random.Range(0, walkSounds.Count)];
    }

    public AudioClip GetRunSound()
    {
        return runSounds[Random.Range(0, runSounds.Count)];
    }


    public AudioClip GetLandSound()
    {
        return landSounds[Random.Range(0, landSounds.Count)];
    }

    public AudioClip GetJumpSound()
    {
        return jumpSounds[Random.Range(0, jumpSounds.Count)];
    }

}
