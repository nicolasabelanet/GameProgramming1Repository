using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAudio : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private AudioSource source;

    [Header("Sounds")]
    [SerializeField] private AudioClip hurtSound;
    [SerializeField] private AudioClip attackSound;

    [Header("Sound Properties")]
    [SerializeField] private float pitchMin;
    [SerializeField] private float pitchMax;

    public void PlayHurtSound()
    {
        PlaySoundOnce(hurtSound); 
    }

    public void PlayAttackSound()
    {
        PlaySoundOnce(attackSound);
    }

    private void PlaySound(AudioClip sound)
    {
        source.clip = sound;
        source.pitch = Random.Range(pitchMin, pitchMax);
        source.Play();
    }

    private void PlaySoundOnce(AudioClip sound)
    {
        source.pitch = Random.Range(pitchMin, pitchMax);
        source.PlayOneShot(sound);
    }

}
