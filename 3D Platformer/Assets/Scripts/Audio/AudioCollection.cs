using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AudioCollection/GenericCollection", fileName = "AudioCollection")]
public class AudioCollection : ScriptableObject
{
    [SerializeField] private List<AudioClip> sounds;

    public AudioClip GetClip()
    {
        return sounds[Random.Range(0, sounds.Count)];
    }

}
