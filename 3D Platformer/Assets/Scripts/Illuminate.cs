using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Illuminate : MonoBehaviour
{

    [SerializeField] private Transform followTarget;
    private Vector3 offset;

    // Start is called before the first frame update
    void Awake()
    {
        offset = transform.position - followTarget.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(followTarget);
    }

}
