using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour
{
    [Header ("Hover Properties")]
    [SerializeField] private float speed = 1f;
    [SerializeField] private float amplitude = 1f;
    [SerializeField] private bool hover = false;

    private float counter = 0;
    private Vector3 initial;

    // Start is called before the first frame update
    void Awake()
    {
        if (hover)
        {
            StartCoroutine(HoverLoop());
        }

        initial = transform.position + Vector3.up * amplitude;
    }

    public void SetHover(Vector3 position)
    {
        initial = position + Vector3.up * amplitude;
        StartCoroutine(HoverLoop());
    }

    private IEnumerator HoverLoop()
    {
        while (true)
        {
            UpdateHover();
            yield return new WaitForFixedUpdate();
        }
    }

    private void UpdateHover()
    {
        Vector3 targetPos = initial + (Vector3.up * Mathf.Sin(counter)) * amplitude;
        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * speed);
        counter += Time.deltaTime;
        counter %= 360;
    }

}
