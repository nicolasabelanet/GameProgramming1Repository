using UnityEngine.InputSystem;
using UnityEngine;

public class Lab2PlayerController : MonoBehaviour
{
    [SerializeField, Range(0, 100.0f)]
    private float movementMultiplier = 20.0f;

    [SerializeField, Range(0, 100.0f)]
    private float jumpMultiplier = 40.0f;


    private bool right;

    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private Vector2 input;
    private Keyboard key;

    void Awake() {
     
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        key = Keyboard.current;
        input = Vector2.zero;
        right = !sr.flipX;
  
    }

    private void look_left() {
        right = false;
        sr.flipX = true;
    }

    private void look_right() {
        right = true;
        sr.flipX = false;
    }

    private void update_position() {

        Vector2 force = new Vector2(input.x * movementMultiplier, input.y * jumpMultiplier);
   
        rb.AddForce(force);

        if ((input.x < 0) && right) {
            look_left();
        }
        else if ((input.x > 0) && !right) {
            look_right();
        }

    }

    void get_input() {
        
        input = Vector2.zero;

        if (key.aKey.isPressed) {
            input.x = -1;
        }

        if (key.dKey.isPressed) {
            input.x = 1;
        }

        if (key.spaceKey.isPressed) {
            input.y = 1;
        }
    }


    void FixedUpdate() {
        update_position();
    }

    void Update()
    {
        get_input();
    }
}
