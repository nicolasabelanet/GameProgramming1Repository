using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolRigidbody : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> points;

    private Vector3 next_point;

    [SerializeField, Range(0, 100.0f)]
    private float speed = 2f;

    private int current_index = 0;

    private float min_distance = .01f;

    private bool right;

    private Rigidbody2D rb;
    private SpriteRenderer sr;

    void Awake() {
        if (points.Count > 0){
            next_point = points[0].transform.position;
        }
        else {
            next_point = Vector3.zero;
        }
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();

        right = sr.flipX;
    }

    private Vector3 get_next_point() {
        current_index += 1;
        current_index %= points.Count;
        next_point = points[current_index].transform.position;
        return next_point;
    }

    private void look_left() {
        right = false;
        sr.flipX = true;
    }

    private void look_right() {
        right = true;
        sr.flipX = false;
    }

    private void update_position() {

        Vector3 dir = (next_point - transform.position).normalized;

        float step = Time.fixedDeltaTime * speed;

        if (Vector3.Distance(transform.position, next_point) > step)
        {
            rb.MovePosition(transform.position + (dir * step));
        }
        else
        {
            rb.MovePosition(next_point);
        }
       

        Vector2 move_dir = rb.velocity;
        if ((Vector2.Dot(move_dir.normalized, Vector2.right) < 0) && right) {
            look_left();
        }
        else if ((Vector2.Dot(move_dir.normalized, Vector2.right) > 0) && !right) {
            look_right();
        }

        if (Vector2.Distance(transform.position, next_point) < min_distance) {
            next_point = get_next_point();
        }        

    }

    // Update is called once per frame
    void FixedUpdate() {
        update_position();
    }

}
