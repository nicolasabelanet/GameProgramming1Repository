using UnityEngine.InputSystem;
using UnityEngine;

public class SimplePlayerController : MonoBehaviour
{
    [SerializeField, Range(0, 20.0f)]
    private float speed = 2.0f;

    private int counter;

    private bool right;


    private PlayerInput player_controls;

    void Awake() {
        counter = 0;
        right = !GetComponent<SpriteRenderer>().flipX;
        player_controls = new PlayerInput();
    }

    private void OnEnable() {
        player_controls.PlayerMap.Enable();
    }

    private void OnDisable() {
        player_controls.PlayerMap.Disable();
    }

    private void look_left() {
        right = false;
        GetComponent<SpriteRenderer>().flipX = true;
    }

    private void look_right() {
        right = true;
        GetComponent<SpriteRenderer>().flipX = false;
    }

    private void update_position(Vector3 input) {

        Vector3 move = new Vector3(input.x, input.y, 0);
        move *= Time.deltaTime;
        move *= speed; 

        transform.position += move;

        if ((input.x < 0) && right) {
            look_left();
        }
        else if ((input.x > 0) && !right) {
            look_right();
        }

    }

    Vector2 get_input() {
        return player_controls.PlayerMap.Movement.ReadValue<Vector2>();
    }


    void Update()
    {
        Debug.Log(counter++);
        Vector2 input = get_input();
        update_position(input);
        
    }
}
