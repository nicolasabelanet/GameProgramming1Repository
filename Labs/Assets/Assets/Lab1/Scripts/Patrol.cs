using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> points;

    private Vector3 next_point;

    [SerializeField, Range(0, 10.0f)]
    private float speed = 2f;

    private int current_index = 0;

    private float min_distance = .01f;

    private bool right;

    void Awake() {
        if (points.Count > 0){
            next_point = points[0].transform.position;
        }
        else {
            next_point = Vector3.zero;
        }

        right = !GetComponent<SpriteRenderer>().flipX;
    }

    private Vector3 get_next_point() {
        current_index += 1;
        current_index %= points.Count;
        next_point = points[current_index].transform.position;
        return next_point;
    }

    private void look_left() {
        right = false;
        GetComponent<SpriteRenderer>().flipX = true;
    }

    private void look_right() {
        right = true;
        GetComponent<SpriteRenderer>().flipX = false;
    }

    private void update_position(){
        
        float step = speed * Time.deltaTime;
        Vector3 move = Vector3.MoveTowards(transform.position, next_point, step);
        transform.position = move;

        Vector3 move_dir = next_point - transform.position;
        if ((Vector3.Dot(move_dir.normalized, Vector3.right) < 0) && right) {
            look_left();
        }
        else if ((Vector3.Dot(move_dir.normalized, Vector3.right) > 0) && !right) {
            look_right();
        }

        if (Vector3.Distance(transform.position, next_point) < min_distance) {
            next_point = get_next_point();
        }        

    }

    // Update is called once per frame
    void Update() {
        update_position();
    }

}
