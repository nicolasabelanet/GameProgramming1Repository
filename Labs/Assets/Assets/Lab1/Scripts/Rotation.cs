using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{

    [SerializeField, Range(-100.0f, 100.0f)]
    private float speed = 25.0f;
    private float current_rotation = 0.0f;

    private void update_rotation() {
        float step = speed * Time.deltaTime;
        current_rotation += step;

        Vector3 euler_rotation = new Vector3(0, 0, current_rotation);
        Quaternion rotation = Quaternion.Euler(euler_rotation);
        transform.rotation = rotation;
    }

    // Update is called once per frame
    void Update()
    {
        update_rotation();
    }
}
