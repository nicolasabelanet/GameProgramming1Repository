using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

public class InClassController : MonoBehaviour
{

    private Keyboard key;

    [SerializeField]
    private float forceMultiplier = 2.0f;

    [SerializeField]
    private Vector2 direction;

    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        key = Keyboard.current;
    }

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        bool somethingPressed = false;

        if (key.aKey.isPressed)
        {
            direction = new Vector2(-1.0f, 0.0f);
            somethingPressed = true;
        }

        if (key.dKey.isPressed)
        {
            direction = new Vector2(1.0f, 0.0f);
            somethingPressed = true;
        }

        if (!somethingPressed)
        {
            direction = Vector2.zero;
        }

    }

    private void FixedUpdate() {
        rb.AddForce(direction * forceMultiplier);
    }

}
